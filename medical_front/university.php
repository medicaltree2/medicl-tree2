
<?php include_once'./include/db.php'?>
<?php include_once'./include/functions.php'?>

<?php
    if(isset($_GET['id'])) {
        $country_id = $_GET['id'];
    } else {
        $country_id = 1;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MEDICAL TREE</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <?php include_once'include/stylesheet.php'?>

    <style>
    .hauto {
        height: auto !important;
    }
    
.flip-card {
  background-color: transparent;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  text-align: center;
  transition: transform 0.6s;
  transform-style: preserve-3d;
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(360deg);
}

.form-box{
  background-color:rgba(0,0,0,.8);
  padding:10px;
  color:white;
}

.form-box input{
  margin-top:10px;
  margin-bottom:10px;
  background-color:transparent;
  color:white;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}


.image:hover .middle {
  opacity: 1;
}

.text {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  padding: 16px 32px;
}

    </style>
</head>

<body>

    <!--==========================
    Header
  ============================-->
    <?php include_once'include/header.php'?>

    <?php $country_name = country_university($con, $country_id);?>
<?php $country = country_name_single($con, $country_id);?>
    <!--==========================
    Intro Section
  ============================-->
    <section id="intro">
        <div class="intro-container">
            <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel" >

                

                <div class="carousel-inner"  role="listbox">
          
                <div class="carousel-item active"  data-interval="1600">
            <div><img class="slider" src="<?php echo $baseImgUrl.'countries/'.$country['image']; ?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                  <div class="row">
                    <div class="col-sm-12">
                      <h1 class="text-white carousel-heading font-weight-bold"><?php echo $country['country']; ?></h1>
                      
                    </div>
                  </div>
              </div>
            </div>
          </div>

                    <!-- <div class="carousel-item active" data-interval="1600">
            <div class="carousel-background"><img src="img/intro-carousel/1.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h1 class="text-white carousel-heading">Best Education <strong>In Germany</strong></h1> 
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
                    <!-- <a href="#featured-services" class="font-weight-bold btn-get-started scrollto mt-4">Get Started</a>
              </div>
            </div>
          </div> -->



                    <!-- <div class="carousel-item">
            <div class="carousel-background"><img src="img/intro-carousel/4.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Nam libero tempore</h2>
                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum.</p>
                <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="img/intro-carousel/5.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Magnam aliquam quaerat</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
              </div>
            </div>
          </div> -->

                </div>

                <!-- <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->

                

            </div>
        </div>
    </section><!-- #intro -->

    <main id="main">
    
        <!--==========================
      Featured Services Section
    ============================-->
        <!-- <section id="featured-services">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 box">
            <i class="ion-ios-bookmarks-outline"></i>
            <h4 class="title"><a href="">Lorem Ipsum Delino</a></h4>
            <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>

          <div class="col-lg-4 box box-bg">
            <i class="ion-ios-stopwatch-outline"></i>
            <h4 class="title"><a href="">Dolor Sitema</a></h4>
            <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
          </div>

          <div class="col-lg-4 box">
            <i class="ion-ios-heart-outline"></i>
            <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
            <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
          </div>

        </div>
      </div>
    </section>#featured-services -->

        <!--==========================
      About Us Section
    ============================-->
        <!-- <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>About Us</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </header>

        <div class="row about-cols">

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="img/about-mission.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Mission</a></h2>
              <p>
              Medical Tree specialized in medical admission in different countries outside India.
Best MBBS India & Abroad Consultants- transparent and complete information guaranteed.
We have brought the concept of MBBS Medical Course across the globe to the doorstep of every student, by making it affordable and devoid of cumbersome procedures.              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-plan.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-list-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Plan</a></h2>
              <p>
              Medical Tree specialized in medical admission in different countries outside India.
Best MBBS India & Abroad Consultants- transparent and complete information guaranteed.
We have brought the concept of MBBS Medical Course across the globe to the doorstep of every              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-vision.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-eye-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Vision</a></h2>
              <p>
              Free counselling for the right University and Country to meet your budget.
Our goals is to understand the basic requirements of the students and help them to choose the right university/college accordingly to build their future.
We give an eye to detail during the process of counselling, admission, documentation etc.              </p>
            </div>
          </div>

        </div>

      </div>
    </section>#about -->

        <!--==========================
      Services Section
    ============================-->
        <section id="services">
            <section class="container-fluid">
                <section class="container">
                    <header class="section-header wow fadeInUp mb-5">
                        <h3>UNIVERSITIES </h3>
                    </header>
                    <section class="row">
             
                    <?php foreach($country_name as $country_name): ?>
                    <a class="col-lg-3 col-md-6 col-sm-6 none-a" href="university-page.php?id=<?php echo $country_name['id'];?>">
        <section class="text-center">
            <figure class="flip-card">
            <img src="<?php echo $baseImgUrl."country_university/".$country_name['university_images'];?>" class="flip-card-inner img-fluid" style="height: 200px !important">
            </figure>
            <h5 class="mt-4 font-weight-bold"><?=$country_name['university_name'];?></h5>  
        </section>
        </a>   
        <?php endforeach; ?>
                     
                    </section>
                	<!-- <div class="col-sm-12 text-center" style="margin-top:30px;">
                        <a class="none-a" href="services.php">
                          <button type="button" class="btn font-weight-bold text-uppercase ggsBtn a">View More</button>
                        </a>
                      </div> -->
                </section>
            </section>
        </section>
        <!--====
    <!-- <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>what we ofer</h3>
<p>Our knowledge, professional behavior, commitment, infrastructure and a highly experienced team shall be in achieving objective of Students Satisfaction.
Lowest admission processing fees and best guidance for MBBS in abroad.
We will provide you all college details and college trips with an expert advice for the medical college of your preferred selection.
For 100% confirmed MBBS seats and MBBS admission choose us</p>        
</header>

        <div class="row">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
            <h4 class="title"><a href="">Lorem Ipsum</a></h4>
            <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
            <h4 class="title"><a href="">Dolor Sitema</a></h4>
            <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-paper-outline"></i></div>
            <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
            <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
            <h4 class="title"><a href="">Magni Dolores</a></h4>
            <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-barcode-outline"></i></div>
            <h4 class="title"><a href="">Nemo Enim</a></h4>
            <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline"></i></div>
            <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
            <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
          </div>

        </div>

      </div>
    </section>#services -->

        <!--==========================
      Call To Action Section
    ============================-->
       

        <!--==========================
      Skills Section
    ============================-->
        <!-- <section id="skills">
      <div class="container">

        <header class="section-header">
          <h3>Our Skills</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        </header>

        <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">90%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">55%</i></span>
            </div>
          </div>

        </div>

      </div>
    </section>

      Facts Section
    ============================-->
        <!-- <section id="facts"  class="wow fadeIn">
      <div class="container">

        <header class="section-header">
          <h3>Facts</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </header>

        <div class="row counters">

  				<div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">274</span>
            <p>Clients</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">421</span>
            <p>Projects</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,364</span>
            <p>Hours Of Support</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>Hard Workers</p>
  				</div>

  			</div>

        <div class="facts-img">
          <img src="img/facts-img.png" alt="" class="img-fluid">
        </div>

      </div>
    </section>#facts -->

        <!--==========================
      Portfolio Section
    ============================-->
      

        <!--==========================
      Clients Section
    ============================-->
        <!-- <section id="clients" class="wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Our Clients</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="img/clients/client-1.png" alt="">
          <img src="img/clients/client-2.png" alt="">
          <img src="img/clients/client-3.png" alt="">
          <img src="img/clients/client-4.png" alt="">
          <img src="img/clients/client-5.png" alt="">
          <img src="img/clients/client-6.png" alt="">
          <img src="img/clients/client-7.png" alt="">
          <img src="img/clients/client-8.png" alt="">
        </div>

      </div>
    </section>#clients -->

        <!--==========================
      Clients Section
    ============================-->



        <!-- <section id="testimonials" class="wow fadeInUp">
            <div class="container">

                <header class="section-header">
                    <h3>Testimonials</h3>
                </header>

                <div class="owl-carousel testimonials-carousel" id="testimonal"> -->






        <!-- <div class="testimonial-item">
            <div class="test-img-position">
            <img src="img/testimonial/user01.jpg" class="testimonial-img" alt="">
            </div>
            <h3>Saurav Nanda</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Some of consultants assured me an admission in a top German Public University where my eligibility criteria was not matching, All this ended up after my visa rejection. RHEIN helped me to come out from this lot of mess, which was created and guided me with the correct pathway pertaining to my course & also, language Institution guidance, as well as got my admissions done and resulting in grant of my visa.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
          <div class="test-img-position">
            <img src="img/testimonial/user02.jpg" class="testimonial-img" alt="">
            </div>
            <h3>Vishal Singh</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              I did a lot of research in search of the best Course (in Engineering) & Institution in Germany and I was stretched and concerned for my schedule, RHEIN, all the way helped me save a  year by instantaneously getting me aware of pathway for admission to a public university of my choice and the course of my preference. As well as getting me a visa in the speculated time frame and put me on a plane right before my orientation week in my dream country. I wish them all the very best.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
          <div class="test-img-position">
            <img src="img/testimonial/user03.jpg" class="testimonial-img" alt="">
            </div>
            <h3>Farhan Anjum</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Getting into a top university is much easier to dream than reality, I was baffled for a couple of months with the information on “How to get into Top Ranked universities” over the internet. The whole thing went smoothly from Admissions to Visa at RHEIN.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
          <div class="test-img-position">
            <img src="img/testimonial/user04.jpg" class="testimonial-img" alt="">
            </div>
            <h3>Naman Kumar</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Adventurous, knowledgeable, fruitful and excitement, these four words describe my experience in University of Germany. Exposure to the extremely passionate teachers and students has made me more passionate about realizing my objectives in life.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
          <div class="test-img-position">
            <img src="img/testimonial/user05.jpg" class="testimonial-img" alt="">
            </div>
            <h3>Prerit</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              I am glad that I approached RHEIN KONSULTANT, who are only trusted in International Education in Germany, well in time. The RHEIN’S staff treats each and every student very seriously and hold themselves accountable towards every student equally. I would want to thank RHEIN from the bottom of my heart, for what I am today.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>
        </div> -->

        </div>

        </div>
        </section><!-- #testimonials -->
        <!-- <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
  <script>
  $.ajax({
        url: "http://127.0.0.1:8000/api/front-testimonial",
        method: "get",
        success: function(data) {
       
            var testimonal = "";
            for (let index = 0; index < data.data.length; index++) {
                // var active="";
                // if(index == 0)
                // {
                //   active ="active";
                // }
                testimonal += `
        <div class="testimonial-item">
            <div class="test-img-position">
            <img src="http://127.0.0.1:8000/image/${data.data[index].image}" class="testimonial-img" alt="">
            </div>
            <h3>${data.data[index].client_name}</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              ${data.data[index].client_msg}             
               <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>
          `;
            }

            $('#testimonal').html(testimonal);
            console.log(testimonal);
        }
    });
  </script>   -->
        <!--==========================
      Contact Section
    ============================-->
    
      

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="text-center remove-margin"><strong> Quick Enquiry Form</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="form_result"></div>
          <form id="formdata">
              <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name">
              </div>
              <div class="form-group">
                <input type="mobile" name="phone" class="form-control" placeholder="Phone Number">
            </div>
              <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="Email Address">
              </div>
              <div class="form-group">
              <textarea  name="msg" class="form-control" placeholder="Enter a message"></textarea>

              
                <!-- <select name="option" id="servicelist" class="form-control">
                <option>choose services</option> -->
                 
                    
 
                <!-- </select> -->
            </div>
          
          <div class="form-group">
            <input  type="submit"  class="btn btn-success btn-block" value="Submit Query">
          </div>
          </form>
      
      </div>

    </div>






<!-- #contact -->

        <!--==========================
      Team Section
    ============================-->
        <!-- <section id="team">
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3>Gallery</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 wow fadeInUp">
            <div class="member">
              <img src="img/team-1.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Walter White</h4>
                  <span>Chief Executive Officer</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="img/team-2.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Sarah Jhonson</h4>
                  <span>Product Manager</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="img/team-3.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>William Anderson</h4>
                  <span>CTO</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div class="member">
              <img src="img/team-4.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Amanda Jepson</h4>
                  <span>Accountant</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>#team -->



    </main>
    <!-- <div id="loading" class="loading">Loading&#8230;</div> -->


    <!--==========================
    Footer
  ============================-->
    <?php include_once'include/footer.php'?>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->

    <!-- JavaScript Libraries -->
    <?php include_once'include/script.php'?>
    <script>
  //   var array = "";
  //   $(document).ready(function(){
  //    setTimeout(function(){   $("#myModal").modal('show'); }, 3000);
  
  // });
    
    
   // var loader = document.getElementById("loading");

    //loader.style.display = "block";
    // $.ajax({
    //     url: baseUrl + 'home',
    //     method: "get",
    //     success: function(data) {
    //        console.log(data);
    //         array = data.data;
    //         var banner = "";
    //         var carousel="";
    //         for (let index = 0; index < data.data.banner.length; index++) {
    //             var active = "";
    //             if (index == 0) {
    //                 active = "active";
    //             }
    //             carousel +=` <li data-target="#introCarousel" data-slide-to="${index}" class="${active}"></li>
    //            `;
    //             banner += `
    //     <div class="carousel-item ${active}"  data-interval="1600">
    //         <div><img class="slider" src="${baseUrl_image+data.data.banner[index].banner_image}" alt=""></div>
    //         <div class="carousel-container">
    //           <div class="carousel-content">
    //               <div class="row">
    //                 <div class="col-sm-12">
    //                   <h1 class="text-white carousel-heading font-weight-bold">${data.data.banner[index].banner_caption} </h1>
                      
    //                 </div>
    //               </div>
    //           </div>
    //         </div>
    //       </div>
    //       `;
    //         }
    //         $('.carousel-indicators').html(carousel);
    //         $('#banner').html(banner);
            // console.log(banner);

       

    
    
    $('#formdata').on('submit', function(event){
   event.preventDefault();
      $.ajax({ 
   url:baseUrl+"query",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
      // console.log(data);
     var html = '';
     if(data.status == '500') 
     {
      html = '<div class="container text-danger">';
      for(var count = 0; count < data.error.length; count++)
      {
       html += '<li>' + data.error[count] + '</li>';
      }
      html += '</div>';
     }
     if(data.status == '200')
     {
      html = '<div class="alert alert-success">' + data.message + '</div>';
      $('#formdata')[0].reset();
     
       setTimeout(function(){ $('#myModal').modal('hide') }, 4000);
      }
    
     // console.log(html);
     $('#form_result').html(html);
    }
   })
  
 });
    
    
   
   
   
   
    
    
    
    
    

   
    </script>

</body>

</html>