
<?php include_once'./include/db.php'?>
<?php include_once'./include/functions.php'?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>MEDICAL TREE</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <?php include_once'include/stylesheet.php'?>


  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <?php include_once'include/header.php'?>


  
    <!--==========================
      Services Section
    ============================-->
    <section id="services">
            <section class="container-fluid">
                <section class="container">
                    <header class="section-header wow fadeInUp" style="margin-top: 135px;">
                        <h3>Study MBBS / PG In Abroad </h3>
                    </header>
                    <section class="row">
                    <?php $allcountry = Allcountry($con) ?>
             
             <?php foreach($allcountry as $allcountry): ?>
                    <a class="col-lg-3 col-md-6 col-sm-6 none-a" href="university.php?id=<?php echo $allcountry['id'];?>">
        <section class="text-center">
            <figure class="flip-card">
            <img src="<?php echo $baseImgUrl.'countries/'.$allcountry['image']; ?>" class="flip-card-inner" width="250px">
            </figure>
            <h5 class="mt-4 font-weight-bold"><?php echo $allcountry['country'];?> </h5>  
        </section>
        </a>   
        <?php endforeach; ?>
                     
                    </section>
                	<!-- <div class="col-sm-12 text-center" style="margin-top:30px;">
                        <a class="none-a" href="services.php">
                          <button type="button" class="btn font-weight-bold text-uppercase ggsBtn a">View More</button>
                        </a>
                      </div> -->
                </section>
            </section>
        </section>

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="text-center remove-margin"><strong> Quick Enquiry Form</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div id="form_result"></div>
            <form id="formdata">
               <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name">
               </div>
               <div class="form-group">
                  <input type="mobile" name="phone" class="form-control" placeholder="Phone Number">
               </div>
               <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="Email Address">
               </div>
               <div class="form-group">
              <textarea  name="msg" class="form-control" placeholder="Enter a message"></textarea>

              
                <!-- <select name="option" id="servicelist" class="form-control">
                <option>choose services</option> -->
                 
                    
 
                <!-- </select> -->
            </div>
               <div class="form-group">
                  <input  type="submit"  class="btn btn-success btn-block" value="Submit Query">
               </div>
            </form>
         </div>
      </div>
      </div>
  </div>
     

      <?php include_once'include/footer.php'?>
       <!-- <div id="loading" class="loading">Loading&#8230;</div> -->


    
      <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
      <!-- Uncomment below i you want to use a preloader -->
      <!-- <div id="preloader"></div> -->
    
      <!-- JavaScript Libraries -->
      <?php include_once'include/script.php'?>

      <script>
       // var loader = document.getElementById("loading");

   // loader.style.display = "block";
    $.ajax({
        url: baseUrl +"front-service",
        method: "get",
        success: function(data) {
           
          
            //  console.log(data);

            var logo = "";
            // for (let index = 0; index < data.data.logo.length; index++) {

            logo += `
      <a class="navbar-brand p-0 m-0" href="index.php">
        <img src="${baseUrl_image+data.data.logo.image}" alt="Brand logo" width="60">
         </a>
       `;

            // }
            $('#logo').html(logo);
            $('#footerlogo').html(logo);

            // console.log(logo);


    //  console.log(data);

      //       var service = "";
      //       var serviceList="";
      //   var servicelist="<option hidden>Choose Service</option>";
      //       for (let index = 0; index < data.data.service.length; index++) {

      //         // var des = ;
      //         //   var des = des.substr(0,100);
      //         //   console.log(des);

      //         serviceList += `
      //           <li><a href="services-page.php?id=${data.data.service[index].id}">${data.data.service[index].heading} </a></li>
      //           `;

      //         servicelist += `
      //             <option>${data.data.service[index].heading}</option>
      //             `;
             
      //           service += `
      //           <div class="col-12 col-sm-6 box wow bounceInUp" data-wow-duration="1.4s">
      //       <a class="none-a" href="services-page.php?id=${data.data.service[index].id}">
      //       <div class="text-center">
      //       <img src="${baseUrl_image+data.data.service[index].image}" width="70px">
      //       </div>
      //         <h4 class="text-center ml-0 mt-4 title service-hover">${data.data.service[index].heading}</h4>
      //         <p class="text-justify ml-0 description">${data.data.service[index].short_description}</p>
      //         </a>
      //       </div>
      //  `;
      //       }

      //     // serviceList += `
      //     //       <li><a href="" class="text-uppercase" >Job Seeker Visa </a></li>
      //     //       `;    
      //       $('#service_list').html(service);
      //       $('#serviceList').html(serviceList);
      //      $('#servicelist').html(servicelist);

            var courseList ="";
            for (let index = 0; index < data.data.course.length; index++) {

            courseList += `
                <li><a href="course-page.php?id=${data.data.course[index].id}">${data.data.course[index].course_name} </a></li>
                `;
            }
        
            $('#courseList').html(courseList);

            // console.log(service);
              loader.style.display = "none";
        }
    });
      
               $('#formdata').on('submit', function(event){
   event.preventDefault();
      $.ajax({ 
   url:baseUrl+"query",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
      // console.log(data);
     var html = '';
     if(data.status == '500') 
     {
      html = '<div class="container text-danger">';
      for(var count = 0; count < data.error.length; count++)
      {
       html += '<li>' + data.error[count] + '</li>';
      }
      html += '</div>';
     }
     if(data.status == '200')
     {
      html = '<div class="alert alert-success">' + data.message + '</div>';
      $('#formdata')[0].reset();
     
       setTimeout(function(){ $('#myModal').modal('hide') }, 4000);
      }
    
     // console.log(html);
     $('#form_result').html(html);
    }
   })
  
 });

      
    </script>

    
    </body>
    </html>