<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <span id="footerlogo"></span>
            <p>Helping you to achieve your dream university !</p>

            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/Free-study-in-Germany-108144927260054/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/tree.medical/" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
      </div>
      
           <div class="col-lg-4 col-md-6 footer-links">
            <h4>site map</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="index.php">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="about.php">About us</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="country.php">MBBS in Abroad</a></li>
              <!-- <li><i class="ion-ios-arrow-right"></i> <a href="courses.php">Courses</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="gallery.php">Gallery</a></li> -->
            </ul>
          </div>

<!--           <div class="col-lg-3 col-md-6 footer-links">
            <h4>quick Links</h4>
            <ul>
          
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Q/A</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div> -->

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p >                            
              <ion-icon name="pin" class="footericon-asset"></ion-icon><span class="font-weight-bold">INDIA:</span>

            54/4, 2nd Floor, Ashok Nagar <br>
             New Delhi 110018<br>
            Landmark : Swasthik Hospital <br>
            Near Tandoor Chowk<br>
            
<!--             
              <ion-icon name="pin" class="footericon-asset"></ion-icon><span class="font-weight-bold">GERMANY:</span>
            Endenicher Str. 14, 53115 Bonn<br>
            Germany<br> -->
            
              <strong> <ion-icon name="call" class="footericon-asset"></ion-icon>  </strong> +91 8851972958  +91  9717856115<br>
              <strong> <ion-icon name="mail" class="footericon-asset"></ion-icon>  </strong> medicaltree.delhi@gmail.com<br>
<!--              <strong> <ion-icon name="mail" class="footericon-asset"></ion-icon>  </strong> gswalia@ggsoverseas.com<br>
             <strong> <ion-icon name="mail" class="footericon-asset"></ion-icon>  </strong> gkwalia@ggsoverseas.com<br> -->
            </p>

            

          </div>

          <!-- <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div> -->

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
      <!-- All Rights Reserved | Copyright 2014 - 2019 @ GGS Overseas -->

        &copy;  COPYRIGHT © 2019. DESIGNED AND DEVELOPED BY TEAM SMDEVOPS
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- #footer -->

<!-- <div class="container-fluid cta-bg py-2 text-center text-white">
  <div class="container">
    <small>
    We are currently updating our website to serve you better. Sorry for any inconvenience caused.
    </small>
  </div>
</div> -->