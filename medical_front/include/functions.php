<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function country_name($con) {
    $sql = "select * from country_name";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function country_name_single($con, $id) {
    $sql = "select * from country_name where id=$id";
    $query = mysqli_query($con, $sql);
    $data = binder($query)[0];
    return $data;
}

function country_university($con, $id) {
	$sql = "select * from country_universities where country_id = $id";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function university_name($con, $id) {
	$sql = "select * from country_universities where id = $id";
    $query = mysqli_query($con, $sql);
    $data = binder($query)[0];
    return $data;
}
function university_image($con, $id) {
	$sql = "select * from country_universities where id = $id";
    $query = mysqli_query($con, $sql);
    $data = binder($query)[0];
    return $data;
}

function university_info($con, $id) {
	$sql = "select * from university_info where university_id = $id";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function uni_img_name($con, $id) {
	$sql = "select * from country_universities where country_id = $id";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function getAppDescription($con) {
	$sql = "select * from app_description";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function getPricing($con) {
	$sql = "select * from pricing_plan";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function getTeam($con) {
	$sql = "select * from team";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function getwhyus($con) {
    $sql = "select * from whyus";
    $query = mysqli_query($con, $sql);
    $data = binder($query)[0];
    return $data;
}

function getwhyusinfo($con) {
    $sql = "select * from why_info";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function getContact($con) {
    $sql = "select * from contact_info";
    $query = mysqli_query($con, $sql);
    $data = binder($query)[0];
    return $data;
}

function getStats($con) {
    $sql = "select * from statistics";
    $query = mysqli_query($con, $sql);
    $data = binder($query)[0];
    return $data;
}

function binder($result) {
	$l= array();
	$loop = 0;
	while ($row = mysqli_fetch_assoc($result)) {
		$l[$loop] = $row;
		$loop = $loop + 1;
	}
	return $l;
}


//for banner
function banner_image($con) {
    $sql = "select * from banner";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function country($con) {
    $sql = "select * from country_name limit 4";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function Allcountry($con) {
    $sql = "select * from country_name";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}


function about($con) {
    $sql = "select * from about";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}

function logo($con) {
    $sql = "select * from logo";
    $query = mysqli_query($con, $sql);
    $data = binder($query);
    return $data;
}
?>
