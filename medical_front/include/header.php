<?php include_once'./include/db.php'?>
<?php include_once'./include/functions.php'?>
<?php
$directoryURI = $_SERVER['REQUEST_URI'];
$path = parse_url($directoryURI, PHP_URL_PATH);
$components = explode('/', $path);
$first_part = $components[1];
  //echo trim($first_part);
//  echo "working";
?>



<!-- <div class="container-fluid cta-bg py-2 text-center text-white fixed-top new-z-index">
  <div class="container">
    <p class="font-weight-bold m-0">
    We are currently updating our website to serve you better. Sorry for any inconvenience caused.
    </p>
  </div>
</div> -->


<!-- <header class="nav-bg"  id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
  
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
        <li  class="<?= ($first_part == 'index.php') ? 'menu-active':''; ?>"><a href="index.php">Home</a></li>
          <li class="<?= ($first_part == 'about.php') ? 'menu-active':''; ?>"><a href="about.php">About Us</a></li>
          <li class="menu-has-children <?= ($first_part == 'services.php') ? 'menu-active':''; ?>"><a href="services.php">Service</a>
            <ul id="serviceList">
            <li>Working</li>
             </ul>
          </li>


          <li class="menu-has-children <?= ($first_part == 'courses.php') ? 'menu-active':''; ?>"><a href="courses.php">Course</a>
            <ul id="courseList">
          
            </ul>
          </li>
          <li class="<?= ($first_part == 'gallery.php') ? 'menu-active':''; ?>"><a href="gallery.php">gallery</a></li>


     
        </ul>
      </nav>
    </div>
  </header>
 -->


 

  <nav class="navbar navbar-expand-lg navbar-dark bg-light  fixed-top">
  <div class="container-fluid">
  <div id="logo" class="pull-left">
  <a class="navbar-brand  p-0 m-0" href="index.php">
  <?php $logo = logo($con) ?>
             
             <?php foreach($logo as $logo): ?>
        <img src="<?php echo $baseImgUrl.'logo/'.$logo['logo_image']; ?>" alt="Brand logo" width="60">
        <?php endforeach; ?>
         </a>   </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item <?= ($first_part == 'index.php') ? 'menu-active':''; ?>">
        <a class="nav-link text-uppercase nav-tab-style" href="index.php">Home</a>
      </li>
      <li class="nav-item  <?= ($first_part == 'about.php') ? 'menu-active':''; ?>">
        <a class="nav-link text-uppercase nav-tab-style" href="about.php">about us</a>
      </li>
     

      <li class="nav-item dropdown text-nowrap nav-tab-style
        <?= ($first_part == 'country.php') ? 'menu-active':''; ?>">
        <a class="nav-link dropdown-toggle text-uppercase nav-tab-style" href="country.php" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
          MBBS in Abroad 
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <?php $allcountry = Allcountry($con) ?>
             
             <?php foreach($allcountry as $allcountry): ?>
          <ul class="no-style pl-2 pb-2">
          <li><a href="university.php?id=<?php echo $allcountry['id'];?>"><?php echo $allcountry['country'];?></a></li>

          </ul>

          <?php endforeach; ?>
        </div>
      </li>

 

     
        <!-- <li class="nav-item  <?= ($first_part == 'about.php') ? 'menu-active':''; ?>">
        <a class="nav-link text-uppercase nav-tab-style" href="index.php#india"> MBBS in India </a>
      </li>
      
      <li class="nav-item <?= ($first_part == 'gallery.php') ? 'menu-active':''; ?>">
        <a class="nav-link text-uppercase nav-tab-style" href="gallery.php">FAQ</a>
      </li> -->
      <li class="nav-item ml-sm-2 <?= ($first_part == 'gallery.php') ? 'menu-active':''; ?>">
       <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
  			Enquiry
		</button>
      </li>
    </ul>
  </div>
  </div>
</nav>