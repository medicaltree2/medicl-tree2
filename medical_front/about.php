<?php include_once'./include/db.php'?>
<?php include_once'./include/functions.php'?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>MEDICAL TREE</title>
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <meta content="" name="keywords">
      <meta content="" name="description">
      <!-- Favicons -->
      <?php include_once'include/stylesheet.php'?>
      <!-- =======================================================
         Theme Name: BizPage
         Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
         Author: BootstrapMade.com
         License: https://bootstrapmade.com/license/
         ======================================================= -->
   </head>
   <body>
      <!--==========================
         Header
         ============================-->
      <?php include_once'include/header.php'?>
      <!--==========================
         about Section
         ============================-->
      <section id="about" class="section-bg header-position">
         <div class="container">
            <header class="section-header">
               <h3>About Us</h3>
               <p class="text-justify">MEDICAL TREE is the leading education & admission consultants in India providing best career counseli in India and Abroad. Direct Admission for Bachelors , Masters , PG in India and Abroad.</p>
            </header>
            <div class="row about-cols" id="about_list">
            <?php $about = about($con) ?>
             
             <?php foreach($about as $about): ?>
            <div class="col-md-4 wow fadeInUp">
               <div class="about-col">
                 <div class="img">
                   <img src=" <?php echo $baseImgUrl.'about/'.$about['image']; ?>" alt="" class="img-fluid">
                 </div>
                 <h2 class="title"><a href="#"><?php echo $about['heading'];?> </a></h2>
                 <?php echo $about['description'];?>
               </div>
             </div>
             <?php endforeach; ?>
               <!-- 
                  <div class="col-md-4 wow fadeInUp">
                    <div class="about-col">
                      <div class="img">
                        <img src="img/about-mission.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                      </div>
                      <h2 class="title"><a href="#">Our Mission</a></h2>
                      <p class="text-center text-justify">
                      Medical Tree specialized in medical admission in different countries outside India.
                  Best MBBS India & Abroad Consultants- transparent and complete information guaranteed.
                  We have brought the concept of MBBS Medical Course across the globe to the doorstep of every student, by making it affordable and devoid of cumbersome procedures.</p>
                    </div>
                  </div>
                  
                  <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="about-col">
                      <div class="img">
                        <img src="img/about-plan.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="ion-ios-list-outline"></i></div>
                      </div>
                      <h2 class="title"><a href="#">Our Plan</a></h2>
                      <p class="text-center text-justify">
                      Medical Tree specialized in medical admission in different countries outside India.
                  Best MBBS India & Abroad Consultants- transparent and complete information guaranteed.
                  We have brought the concept of MBBS Medical Course across the globe to the doorstep of every student, by making it affordable and devoid of cumbersome procedures.</p>
                    </div>
                  </div>
                  
                  <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="about-col">
                      <div class="img">
                        <img src="img/about-vision.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                      </div>
                      <h2 class="title"><a href="#">Our Vision</a></h2>
                      <p class="text-center text-justify">
                      Medical Tree specialized in medical admission in different countries outside India.
                  Best MBBS India & Abroad Consultants- transparent and complete information guaranteed.
                  We have brought the concept of MBBS Medical Course across the globe to the doorstep of every student, by making it affordable and devoid of cumbersome procedures.</p>
                    </div>
                  </div> -->
            </div>
         </div>
      </section>
      <!-- #about -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="text-center remove-margin"><strong> Quick Enquiry Form</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div id="form_result"></div>
            <form id="formdata">
               <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name">
               </div>
               <div class="form-group">
                  <input type="mobile" name="phone" class="form-control" placeholder="Phone Number">
               </div>
               <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="Email Address">
               </div>
               <div class="form-group">
              <textarea  name="msg" class="form-control" placeholder="Enter a message"></textarea>

              
                <!-- <select name="option" id="servicelist" class="form-control">
                <option>choose services</option> -->
                 
                    
 
                <!-- </select> -->
            </div>
               <div class="form-group">
                  <input  type="submit"  class="btn btn-success btn-block" value="Submit Query">
               </div>
            </form>
         </div>
      </div>
      </div>
      </div>
      <!-- <div id="loading" class="loading">Loading&#8230;</div> -->
      <?php include_once'include/footer.php'?>
      <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
      <!-- Uncomment below i you want to use a preloader -->
      <!-- <div id="preloader"></div> -->
      <!-- JavaScript Libraries -->
      <?php include_once'include/script.php'?>
      <script>
      <!-- <script>
        // var loader = document.getElementById("loading");
         
         //loader.style.display = "block";
         // $.ajax({
         //  url: baseUrl + "front-about",
         //  method: "get",
         //  success: function(data) {
             
            
         //       console.log(data);
         
         //      var logo = "";
         //      // for (let index = 0; index < data.data.logo.length; index++) {
         
         //      logo += `
         // <a class="navbar-brand p-0 m-0" href="index.php">
         //  <img src="${baseUrl_image+data.data.logo.image}" alt="Brand logo" width="60">
         //   </a>
         // `;
         
         //      // }
         //      $('#logo').html(logo);
         //      $('#footerlogo').html(logo);
         
         //      // console.log(logo);
         
         
         // console.log(data); 
         
         //      var about = "";
         //      for (let index = 0; index < data.data.about.length; index++) {
         
           
         
         //          about += `
         //          <div class="col-md-4 wow fadeInUp">
         //      <div class="about-col">
         //        <div class="img">
         //          <img src=" ${baseUrl_image+data.data.about[index].image}" alt="" class="img-fluid">
         //          <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
         //        </div>
         //        <h2 class="title"><a href="#">${data.data.about[index].heading} </a></h2>
         //        <p class="text-justify">
         //        ${data.data.about[index].description} </p>
         //      </div>
         //    </div>
         // `;
         //      }
         
         
         
         //      $('#about_list').html(about);
           
         //      var serviceList = "";
         //      var servicelist = "<option hidden>Choose Service</option>";
         //      for (let index = 0; index < data.data.service.length; index++) {
         
         //          serviceList += `
         //          <li><a href="services-page.php?id=${data.data.service[index].id}">${data.data.service[index].heading} </a></li>
         //          `;
         //        servicelist += `
         //          <option>${data.data.service[index].heading}</option>
         //          `;
             
         //      }
         //    // serviceList += `
         //    //       <li><a href="" class="text-uppercase" >Job Seeker Visa </a></li>
         //    //       `;
         
         //      $('#serviceList').html(serviceList);
         //      $('#servicelist').html(servicelist);
         
         //      var courseList ="";
         //      for (let index = 0; index < data.data.course.length; index++) {
         
         //      courseList += `
         //          <li><a href="course-page.php?id=${data.data.course[index].id}">${data.data.course[index].course_name} </a></li>
         //          `;
         //      }
         //      $('#courseList').html(courseList);
         
         //      //console.log(service);
         
         
         //       loader.style.display = "none";
         
         //  }
         // });
      
       $('#formdata').on('submit', function(event){
   event.preventDefault();
      $.ajax({ 
   url:baseUrl+"query",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
      // console.log(data);
     var html = '';
     if(data.status == '500') 
     {
      html = '<div class="container text-danger">';
      for(var count = 0; count < data.error.length; count++)
      {
       html += '<li>' + data.error[count] + '</li>';
      }
      html += '</div>';
     }
     if(data.status == '200')
     {
      html = '<div class="alert alert-success">' + data.message + '</div>';
      $('#formdata')[0].reset();
     
       setTimeout(function(){ $('#myModal').modal('hide') }, 4000);
      }
    
     // console.log(html);
     $('#form_result').html(html);
    }
   })
  
 });

      </script>
   </body>
</html>
