<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>GGSOVERSEAS</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <?php include_once'include/stylesheet.php'?>


  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>


  <!--==========================
    Header
  ============================-->
   <?php include_once'include/header.php'?>


   <!--==========================
      Team Section
    ============================-->

   
    <section id="team" class="section-bg header-position header-position">
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3>Gallery</h3>
          <p>Have a look at our event images that were held recently</p>
        </div>

        <div class="row" id="gallery">
        
        </div>

      </div>
    </section>


    <section  class="section-bg header-position header-position">
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3>Video</h3>
        </div>
        <div class="row" id="video">
      </div>
      </div>
    </section>

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="text-center remove-margin"><strong> Quick Enquiry Form</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div id="form_result"></div>
            <form id="formdata">
               <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name">
               </div>
               <div class="form-group">
                  <input type="mobile" name="phone" class="form-control" placeholder="Phone Number">
               </div>
               <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="Email Address">
               </div>
               <div class="form-group">
                  <select name="option" id="servicelist" class="form-control">
                     <option>choose services</option>
                  </select>
               </div>
               <div class="form-group">
                  <input  type="submit"  class="btn btn-success btn-block" value="Submit Query">
               </div>
            </form>
         </div>
      </div>
      </div>
      </div>
    

      <?php include_once'include/footer.php'?>
       <div id="loading" class="loading">Loading&#8230;</div>


    
      <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
      <!-- Uncomment below i you want to use a preloader -->
      <!-- <div id="preloader"></div> -->
    
      <!-- JavaScript Libraries -->
      <?php include_once'include/script.php'?>
     <script>
	     var loader = document.getElementById("loading");

		    loader.style.display = "block";
    		$.ajax({
       			 url: baseUrl + "front-gallery",
      			  method: "get",
		        success: function(data) {
           
          
             console.log(data);

            var logo = "";
            // for (let index = 0; index < data.data.logo.length; index++) {

            logo += `
      <a class="navbar-brand p-0 m-0" href="index.php">
        <img src="${baseUrl_image+data.data.logo.image}" alt="Brand logo" width="60">
         </a>
       `;

            // }
            $('#logo').html(logo);
            $('#footerlogo').html(logo);

            // console.log(logo);


     console.log(data);

            var gallery = "";
            for (let index = 0; index < data.data.gallery.length; index++) {


                gallery += `
                  <div class="col-lg-3 col-md-6 mt-4 mt-5 ptr animation" data-toggle="modal" data-target="#g01${data.data.gallery[index].id}">
                <img src="${baseUrl_image+data.data.gallery[index].image}" class="img-fluid" alt="">
          </div>


          <div class="modal fade" id="g01${data.data.gallery[index].id}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                <div class="modal-content">
<ion-icon class="position-absolute button-position ptr mt-2 mr-2" name="close-circle"  width="30" data-dismiss="modal" aria-label="Close"></ion-icon>
                    <div class="modal-body p-0">
                    <img src="${baseUrl_image+data.data.gallery[index].image}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
       `;
            }

            
            $('#gallery').html(gallery);

                    var video = "";
            for (let index = 0; index < data.data.video.length; index++) {


                video += `
					<div class="col-lg-3 col-sm-offset-3 col-lg-offset-3  col-md-6 mt-4 mt-5" style="height:200px;width:500px;margin-left:20px;margin-right:20px;">
                       ${data.data.video[index].video_url}
                    </div>`;
  			
            }

            console.log(video);
            $('#video').html(video);


            
        
        var serviceList = "";
              var servicelist = "<option hidden>Choose Service</option>";
              for (let index = 0; index < data.data.service.length; index++) {
         
                  serviceList += `
                  <li><a href="services-page.php?id=${data.data.service[index].id}">${data.data.service[index].heading} </a></li>
                  `;
                servicelist += `
                  <option>${data.data.service[index].heading}</option>
                  `;
             
              }
         
              $('#serviceList').html(serviceList);
              $('#servicelist').html(servicelist);
         
            var courseList ="";
            for (let index = 0; index < data.data.course.length; index++) {

            courseList += `
                <li><a href="course-page.php?id=${data.data.course[index].id}">${data.data.course[index].course_name} </a></li>
                `;
            }
                console.log(courseList);
            $('#courseList').html(courseList);
            


               loader.style.display = "none";
        }
    });
       $('#formdata').on('submit', function(event){
   event.preventDefault();
      $.ajax({ 
   url:baseUrl+"query",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
      // console.log(data);
     var html = '';
     if(data.status == '500') 
     {
      html = '<div class="container text-danger">';
      for(var count = 0; count < data.error.length; count++)
      {
       html += '<li>' + data.error[count] + '</li>';
      }
      html += '</div>';
     }
     if(data.status == '200')
     {
      html = '<div class="alert alert-success">' + data.message + '</div>';
      $('#formdata')[0].reset();
     
       setTimeout(function(){ $('#myModal').modal('hide') }, 4000);
      }
    
     // console.log(html);
     $('#form_result').html(html);
    }
   })
  
 });
     
     
     

     </script>
    </body>
    </html>