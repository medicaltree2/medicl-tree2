<?php
$id = $_GET['id'];


?>
<!doctype html>

<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <?php include_once'include/stylesheet.php'?>

</head>

<body>
<?php include_once'include/header.php'?>

    <div class="container-fluid py-5">
        <div class="container">
       
            <div class="row header-position" id="gallery" >
                <!-- <div class="col-12 col-sm-6">
                    <img src="img/services/university.png" alt="" width="160">
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0 service-position">
                    <h2 class="font-weight-bold text-uppercase">University Selection</h2>
                </div>
                <div class="col-12 col-sm-12 mt-5">
                    <p class="text-justify text-secondary">Selecting a university abroad for education needs requires adequate guidance and expert advice to zero down on a particular country, institute, and study program. Rhein Konsultancy offers one-stop solutions to all queries towards university
                        selection. At Rhein Konsultancy we help students get admissions into universities and colleges that best suit their personal, academic, financial and other parameters. The knowledge and rich experience of our counselors allow students
                        to arrive at the best possible decisions.</p>
                    <p class="text-justify text-secondary">With an increasing number of universities and colleges offering numerous courses selecting the right course and a perfect institute is a big challenge. It is up to students to decide what is his/her priorities.</p>
                </div> -->
            </div>
        </div>
    </div>

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="text-center remove-margin"><strong> Quick Enquiry Form</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div id="form_result"></div>
            <form id="formdata">
               <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name">
               </div>
               <div class="form-group">
                  <input type="mobile" name="phone" class="form-control" placeholder="Phone Number">
               </div>
               <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="Email Address">
               </div>
               <div class="form-group">
              <textarea  name="msg" class="form-control" placeholder="Enter a message"></textarea>

              
                <!-- <select name="option" id="servicelist" class="form-control">
                <option>choose services</option> -->
                 
                    
 
                <!-- </select> -->
            </div>
               <div class="form-group">
                  <input  type="submit"  class="btn btn-success btn-block" value="Submit Query">
               </div>
            </form>
         </div>
      </div>
      </div>
  </div>
     
    <?php include_once'include/footer.php'?>
           <div id="loading" class="loading">Loading&#8230;</div>



    
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
      <!-- Uncomment below i you want to use a preloader -->
      <!-- <div id="preloader"></div> -->
    
      <!-- JavaScript Libraries -->
      <?php include_once'include/script.php'?>

      <script>
         var loader = document.getElementById("loading");

    loader.style.display = "block";
          $.ajax({
        url: baseUrl + "front-service-fully/"+<?=$id?>,
        method: "get",
        success: function(data) {
           
          
             console.log(data);

            var logo = "";
            // for (let index = 0; index < data.data.logo.length; index++) {

            logo += `
      <a class="navbar-brand p-0 m-0" href="index.php">
        <img src="${baseUrl_image+data.data.logo.image}" alt="Brand logo" width="60">
         </a>
       `;

            // }
            $('#logo').html(logo);
            $('#footerlogo').html(logo);

            // console.log(logo);


     console.log(data);

            var gallery = "";
            // for (let index = 0; index < data.data.length; index++) {


                gallery += `
                 <div class="col-12 col-sm-6 text-center text-sm-left">
                    <img src="${baseUrl_image+data.data.service.image}" alt="" width="160">
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0 service-position">
                    <h3 class="font-weight-bold text-uppercase">${data.data.service.heading}</h3>
                </div>
                <div class="col-12 col-sm-12 mt-sm-3">
                    <p class="text-justify ">${data.data.service.description}</p>
                </div>
       `;
            // }

            
            $('#gallery').html(gallery);
      
              var serviceList = "";
              var servicelist = "<option hidden>Choose Service</option>";
              for (let index = 0; index < data.data.services.length; index++) {
         
                  serviceList += `
                  <li><a href="services-page.php?id=${data.data.services[index].id}">${data.data.services[index].heading} </a></li>
                  `;
                servicelist += `
                  <option>${data.data.services[index].heading}</option>
                  `;
             
              }
            // serviceList += `
            //       <li><a href="" class="text-uppercase" >Job Seeker Visa </a></li>
            //       `;
         
              $('#serviceList').html(serviceList);
              $('#servicelist').html(servicelist);
         
            var courseList ="";
            for (let index = 0; index < data.data.course.length; index++) {

            courseList += `
                <li><a href="course-page.php?id=${data.data.course[index].id}">${data.data.course[index].course_name} </a></li>
                `;
            }
            $('#courseList').html(courseList);



             loader.style.display = "none";
        }
    });

       $('#formdata').on('submit', function(event){
   event.preventDefault();
      $.ajax({ 
   url:baseUrl+"query",
    method:"POST",
    data: new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
      // console.log(data);
     var html = '';
     if(data.status == '500') 
     {
      html = '<div class="container text-danger">';
      for(var count = 0; count < data.error.length; count++)
      {
       html += '<li>' + data.error[count] + '</li>';
      }
      html += '</div>';
     }
     if(data.status == '200')
     {
      html = '<div class="alert alert-success">' + data.message + '</div>';
      $('#formdata')[0].reset();
     
       setTimeout(function(){ $('#myModal').modal('hide') }, 4000);
      }
    
     // console.log(html);
     $('#form_result').html(html);
    }
   })
  
 });
      
      </script>
</body>

</html>