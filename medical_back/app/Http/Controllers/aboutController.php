<?php

namespace App\Http\Controllers;

use App\DataTables\aboutDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateaboutRequest;
use App\Http\Requests\UpdateaboutRequest;
use App\Repositories\aboutRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class aboutController extends AppBaseController
{
    /** @var  aboutRepository */
    private $aboutRepository;

    public function __construct(aboutRepository $aboutRepo)
    {
        $this->aboutRepository = $aboutRepo;
    }

    /**
     * Display a listing of the about.
     *
     * @param aboutDataTable $aboutDataTable
     * @return Response
     */
    public function index(aboutDataTable $aboutDataTable)
    {
        return $aboutDataTable->render('abouts.index');
    }

    /**
     * Show the form for creating a new about.
     *
     * @return Response
     */
    public function create()
    {
        return view('abouts.create');
    }

    /**
     * Store a newly created about in storage.
     *
     * @param CreateaboutRequest $request
     *
     * @return Response
     */
    public function store(CreateaboutRequest $request)
    {
        $input = $request->all();
        
        $file=$request->file('image');
        $filename= $file->getClientOriginalName();
        $filename = str_replace(' ', '_', $filename);
        $file->move(public_path('uploads/about'), $filename);
        $input['image'] = $filename;


        $about = $this->aboutRepository->create($input);

        Flash::success('About saved successfully.');

        return redirect(route('abouts.index'));
    }

    /**
     * Display the specified about.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            Flash::error('About not found');

            return redirect(route('abouts.index'));
        }

        return view('abouts.show')->with('about', $about);
    }

    /**
     * Show the form for editing the specified about.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            Flash::error('About not found');

            return redirect(route('abouts.index'));
        }

        return view('abouts.edit')->with('about', $about);
    }

    /**
     * Update the specified about in storage.
     *
     * @param  int              $id
     * @param UpdateaboutRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateaboutRequest $request)
    {
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            Flash::error('About not found');

            return redirect(route('abouts.index'));
        }

        $input = $request->all();

        $file=$request->file('image');
        if($file) {
            $filename= $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $file->move(public_path('uploads/about'), $filename);
            $input['image'] = $filename;    
        }

        $about = $this->aboutRepository->update($input, $id);

        Flash::success('About updated successfully.');

        return redirect(route('abouts.index'));
    }

    /**
     * Remove the specified about from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            Flash::error('About not found');

            return redirect(route('abouts.index'));
        }

        $this->aboutRepository->delete($id);

        Flash::success('About deleted successfully.');

        return redirect(route('abouts.index'));
    }
}
