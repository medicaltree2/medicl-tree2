<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatelogoAPIRequest;
use App\Http\Requests\API\UpdatelogoAPIRequest;
use App\Models\logo;
use App\Repositories\logoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class logoController
 * @package App\Http\Controllers\API
 */

class logoAPIController extends AppBaseController
{
    /** @var  logoRepository */
    private $logoRepository;

    public function __construct(logoRepository $logoRepo)
    {
        $this->logoRepository = $logoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/logos",
     *      summary="Get a listing of the logos.",
     *      tags={"logo"},
     *      description="Get all logos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/logo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $logos = $this->logoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($logos->toArray(), 'Logos retrieved successfully');
    }

    /**
     * @param CreatelogoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/logos",
     *      summary="Store a newly created logo in storage",
     *      tags={"logo"},
     *      description="Store logo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="logo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/logo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/logo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatelogoAPIRequest $request)
    {
        $input = $request->all();

        $logo = $this->logoRepository->create($input);

        return $this->sendResponse($logo->toArray(), 'Logo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/logos/{id}",
     *      summary="Display the specified logo",
     *      tags={"logo"},
     *      description="Get logo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of logo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/logo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var logo $logo */
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            return $this->sendError('Logo not found');
        }

        return $this->sendResponse($logo->toArray(), 'Logo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatelogoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/logos/{id}",
     *      summary="Update the specified logo in storage",
     *      tags={"logo"},
     *      description="Update logo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of logo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="logo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/logo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/logo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatelogoAPIRequest $request)
    {
        $input = $request->all();

        /** @var logo $logo */
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            return $this->sendError('Logo not found');
        }

        $logo = $this->logoRepository->update($input, $id);

        return $this->sendResponse($logo->toArray(), 'logo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/logos/{id}",
     *      summary="Remove the specified logo from storage",
     *      tags={"logo"},
     *      description="Delete logo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of logo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var logo $logo */
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            return $this->sendError('Logo not found');
        }

        $logo->delete();

        return $this->sendSuccess('Logo deleted successfully');
    }
}
