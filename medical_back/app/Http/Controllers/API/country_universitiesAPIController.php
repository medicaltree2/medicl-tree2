<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createcountry_universitiesAPIRequest;
use App\Http\Requests\API\Updatecountry_universitiesAPIRequest;
use App\Models\country_universities;
use App\Repositories\country_universitiesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class country_universitiesController
 * @package App\Http\Controllers\API
 */

class country_universitiesAPIController extends AppBaseController
{
    /** @var  country_universitiesRepository */
    private $countryUniversitiesRepository;

    public function __construct(country_universitiesRepository $countryUniversitiesRepo)
    {
        $this->countryUniversitiesRepository = $countryUniversitiesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/countryUniversities",
     *      summary="Get a listing of the country_universities.",
     *      tags={"country_universities"},
     *      description="Get all country_universities",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/country_universities")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $countryUniversities = $this->countryUniversitiesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($countryUniversities->toArray(), 'Country Universities retrieved successfully');
    }

    /**
     * @param Createcountry_universitiesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/countryUniversities",
     *      summary="Store a newly created country_universities in storage",
     *      tags={"country_universities"},
     *      description="Store country_universities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="country_universities that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/country_universities")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/country_universities"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Createcountry_universitiesAPIRequest $request)
    {
        $input = $request->all();

        $countryUniversities = $this->countryUniversitiesRepository->create($input);

        return $this->sendResponse($countryUniversities->toArray(), 'Country Universities saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/countryUniversities/{id}",
     *      summary="Display the specified country_universities",
     *      tags={"country_universities"},
     *      description="Get country_universities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of country_universities",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/country_universities"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var country_universities $countryUniversities */
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            return $this->sendError('Country Universities not found');
        }

        return $this->sendResponse($countryUniversities->toArray(), 'Country Universities retrieved successfully');
    }

    /**
     * @param int $id
     * @param Updatecountry_universitiesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/countryUniversities/{id}",
     *      summary="Update the specified country_universities in storage",
     *      tags={"country_universities"},
     *      description="Update country_universities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of country_universities",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="country_universities that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/country_universities")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/country_universities"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Updatecountry_universitiesAPIRequest $request)
    {
        $input = $request->all();

        /** @var country_universities $countryUniversities */
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            return $this->sendError('Country Universities not found');
        }

        $countryUniversities = $this->countryUniversitiesRepository->update($input, $id);

        return $this->sendResponse($countryUniversities->toArray(), 'country_universities updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/countryUniversities/{id}",
     *      summary="Remove the specified country_universities from storage",
     *      tags={"country_universities"},
     *      description="Delete country_universities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of country_universities",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var country_universities $countryUniversities */
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            return $this->sendError('Country Universities not found');
        }

        $countryUniversities->delete();

        return $this->sendSuccess('Country Universities deleted successfully');
    }
}
