<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createuniversity_imagesAPIRequest;
use App\Http\Requests\API\Updateuniversity_imagesAPIRequest;
use App\Models\university_images;
use App\Repositories\university_imagesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class university_imagesController
 * @package App\Http\Controllers\API
 */

class university_imagesAPIController extends AppBaseController
{
    /** @var  university_imagesRepository */
    private $universityImagesRepository;

    public function __construct(university_imagesRepository $universityImagesRepo)
    {
        $this->universityImagesRepository = $universityImagesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/universityImages",
     *      summary="Get a listing of the university_images.",
     *      tags={"university_images"},
     *      description="Get all university_images",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/university_images")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $universityImages = $this->universityImagesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($universityImages->toArray(), 'University Images retrieved successfully');
    }

    /**
     * @param Createuniversity_imagesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/universityImages",
     *      summary="Store a newly created university_images in storage",
     *      tags={"university_images"},
     *      description="Store university_images",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="university_images that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/university_images")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/university_images"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Createuniversity_imagesAPIRequest $request)
    {
        $input = $request->all();

        $universityImages = $this->universityImagesRepository->create($input);

        return $this->sendResponse($universityImages->toArray(), 'University Images saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/universityImages/{id}",
     *      summary="Display the specified university_images",
     *      tags={"university_images"},
     *      description="Get university_images",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of university_images",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/university_images"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var university_images $universityImages */
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            return $this->sendError('University Images not found');
        }

        return $this->sendResponse($universityImages->toArray(), 'University Images retrieved successfully');
    }

    /**
     * @param int $id
     * @param Updateuniversity_imagesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/universityImages/{id}",
     *      summary="Update the specified university_images in storage",
     *      tags={"university_images"},
     *      description="Update university_images",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of university_images",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="university_images that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/university_images")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/university_images"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Updateuniversity_imagesAPIRequest $request)
    {
        $input = $request->all();

        /** @var university_images $universityImages */
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            return $this->sendError('University Images not found');
        }

        $universityImages = $this->universityImagesRepository->update($input, $id);

        return $this->sendResponse($universityImages->toArray(), 'university_images updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/universityImages/{id}",
     *      summary="Remove the specified university_images from storage",
     *      tags={"university_images"},
     *      description="Delete university_images",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of university_images",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var university_images $universityImages */
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            return $this->sendError('University Images not found');
        }

        $universityImages->delete();

        return $this->sendSuccess('University Images deleted successfully');
    }
}
