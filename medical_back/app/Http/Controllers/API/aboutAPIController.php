<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateaboutAPIRequest;
use App\Http\Requests\API\UpdateaboutAPIRequest;
use App\Models\about;
use App\Repositories\aboutRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class aboutController
 * @package App\Http\Controllers\API
 */

class aboutAPIController extends AppBaseController
{
    /** @var  aboutRepository */
    private $aboutRepository;

    public function __construct(aboutRepository $aboutRepo)
    {
        $this->aboutRepository = $aboutRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/abouts",
     *      summary="Get a listing of the abouts.",
     *      tags={"about"},
     *      description="Get all abouts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/about")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $abouts = $this->aboutRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($abouts->toArray(), 'Abouts retrieved successfully');
    }

    /**
     * @param CreateaboutAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/abouts",
     *      summary="Store a newly created about in storage",
     *      tags={"about"},
     *      description="Store about",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="about that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/about")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/about"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateaboutAPIRequest $request)
    {
        $input = $request->all();

        $about = $this->aboutRepository->create($input);

        return $this->sendResponse($about->toArray(), 'About saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/abouts/{id}",
     *      summary="Display the specified about",
     *      tags={"about"},
     *      description="Get about",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of about",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/about"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var about $about */
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            return $this->sendError('About not found');
        }

        return $this->sendResponse($about->toArray(), 'About retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateaboutAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/abouts/{id}",
     *      summary="Update the specified about in storage",
     *      tags={"about"},
     *      description="Update about",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of about",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="about that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/about")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/about"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateaboutAPIRequest $request)
    {
        $input = $request->all();

        /** @var about $about */
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            return $this->sendError('About not found');
        }

        $about = $this->aboutRepository->update($input, $id);

        return $this->sendResponse($about->toArray(), 'about updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/abouts/{id}",
     *      summary="Remove the specified about from storage",
     *      tags={"about"},
     *      description="Delete about",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of about",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var about $about */
        $about = $this->aboutRepository->find($id);

        if (empty($about)) {
            return $this->sendError('About not found');
        }

        $about->delete();

        return $this->sendSuccess('About deleted successfully');
    }
}
