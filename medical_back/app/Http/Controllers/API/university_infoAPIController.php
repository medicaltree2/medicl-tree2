<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createuniversity_infoAPIRequest;
use App\Http\Requests\API\Updateuniversity_infoAPIRequest;
use App\Models\university_info;
use App\Repositories\university_infoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class university_infoController
 * @package App\Http\Controllers\API
 */

class university_infoAPIController extends AppBaseController
{
    /** @var  university_infoRepository */
    private $universityInfoRepository;

    public function __construct(university_infoRepository $universityInfoRepo)
    {
        $this->universityInfoRepository = $universityInfoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/universityInfos",
     *      summary="Get a listing of the university_infos.",
     *      tags={"university_info"},
     *      description="Get all university_infos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/university_info")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $universityInfos = $this->universityInfoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($universityInfos->toArray(), 'University Infos retrieved successfully');
    }

    /**
     * @param Createuniversity_infoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/universityInfos",
     *      summary="Store a newly created university_info in storage",
     *      tags={"university_info"},
     *      description="Store university_info",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="university_info that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/university_info")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/university_info"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Createuniversity_infoAPIRequest $request)
    {
        $input = $request->all();

        $universityInfo = $this->universityInfoRepository->create($input);

        return $this->sendResponse($universityInfo->toArray(), 'University Info saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/universityInfos/{id}",
     *      summary="Display the specified university_info",
     *      tags={"university_info"},
     *      description="Get university_info",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of university_info",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/university_info"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var university_info $universityInfo */
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            return $this->sendError('University Info not found');
        }

        return $this->sendResponse($universityInfo->toArray(), 'University Info retrieved successfully');
    }

    /**
     * @param int $id
     * @param Updateuniversity_infoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/universityInfos/{id}",
     *      summary="Update the specified university_info in storage",
     *      tags={"university_info"},
     *      description="Update university_info",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of university_info",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="university_info that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/university_info")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/university_info"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Updateuniversity_infoAPIRequest $request)
    {
        $input = $request->all();

        /** @var university_info $universityInfo */
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            return $this->sendError('University Info not found');
        }

        $universityInfo = $this->universityInfoRepository->update($input, $id);

        return $this->sendResponse($universityInfo->toArray(), 'university_info updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/universityInfos/{id}",
     *      summary="Remove the specified university_info from storage",
     *      tags={"university_info"},
     *      description="Delete university_info",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of university_info",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var university_info $universityInfo */
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            return $this->sendError('University Info not found');
        }

        $universityInfo->delete();

        return $this->sendSuccess('University Info deleted successfully');
    }
}
