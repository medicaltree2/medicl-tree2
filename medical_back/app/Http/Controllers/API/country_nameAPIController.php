<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createcountry_nameAPIRequest;
use App\Http\Requests\API\Updatecountry_nameAPIRequest;
use App\Models\country_name;
use App\Repositories\country_nameRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class country_nameController
 * @package App\Http\Controllers\API
 */

class country_nameAPIController extends AppBaseController
{
    /** @var  country_nameRepository */
    private $countryNameRepository;

    public function __construct(country_nameRepository $countryNameRepo)
    {
        $this->countryNameRepository = $countryNameRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/countryNames",
     *      summary="Get a listing of the country_names.",
     *      tags={"country_name"},
     *      description="Get all country_names",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/country_name")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $countryNames = $this->countryNameRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($countryNames->toArray(), 'Country Names retrieved successfully');
    }

    /**
     * @param Createcountry_nameAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/countryNames",
     *      summary="Store a newly created country_name in storage",
     *      tags={"country_name"},
     *      description="Store country_name",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="country_name that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/country_name")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/country_name"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Createcountry_nameAPIRequest $request)
    {
        $input = $request->all();

        $countryName = $this->countryNameRepository->create($input);

        return $this->sendResponse($countryName->toArray(), 'Country Name saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/countryNames/{id}",
     *      summary="Display the specified country_name",
     *      tags={"country_name"},
     *      description="Get country_name",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of country_name",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/country_name"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var country_name $countryName */
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            return $this->sendError('Country Name not found');
        }

        return $this->sendResponse($countryName->toArray(), 'Country Name retrieved successfully');
    }

    /**
     * @param int $id
     * @param Updatecountry_nameAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/countryNames/{id}",
     *      summary="Update the specified country_name in storage",
     *      tags={"country_name"},
     *      description="Update country_name",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of country_name",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="country_name that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/country_name")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/country_name"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Updatecountry_nameAPIRequest $request)
    {
        $input = $request->all();

        /** @var country_name $countryName */
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            return $this->sendError('Country Name not found');
        }

        $countryName = $this->countryNameRepository->update($input, $id);

        return $this->sendResponse($countryName->toArray(), 'country_name updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/countryNames/{id}",
     *      summary="Remove the specified country_name from storage",
     *      tags={"country_name"},
     *      description="Delete country_name",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of country_name",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var country_name $countryName */
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            return $this->sendError('Country Name not found');
        }

        $countryName->delete();

        return $this->sendSuccess('Country Name deleted successfully');
    }
}
