<?php

namespace App\Http\Controllers;

use App\DataTables\logoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatelogoRequest;
use App\Http\Requests\UpdatelogoRequest;
use App\Repositories\logoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class logoController extends AppBaseController
{
    /** @var  logoRepository */
    private $logoRepository;

    public function __construct(logoRepository $logoRepo)
    {
        $this->logoRepository = $logoRepo;
    }

    /**
     * Display a listing of the logo.
     *
     * @param logoDataTable $logoDataTable
     * @return Response
     */
    public function index(logoDataTable $logoDataTable)
    {
        return $logoDataTable->render('logos.index');
    }

    /**
     * Show the form for creating a new logo.
     *
     * @return Response
     */
    public function create()
    {
        return view('logos.create');
    }

    /**
     * Store a newly created logo in storage.
     *
     * @param CreatelogoRequest $request
     *
     * @return Response
     */
    public function store(CreatelogoRequest $request)
    {
        $input = $request->all();

        $file=$request->file('logo_image');
        $filename= $file->getClientOriginalName();
        $filename = str_replace(' ', '_', $filename);
        $file->move(public_path('uploads/logo'), $filename);
        $input['logo_image'] = $filename;

        $logo = $this->logoRepository->create($input);

        Flash::success('Logo saved successfully.');

        return redirect(route('logos.index'));
    }

    /**
     * Display the specified logo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            Flash::error('Logo not found');

            return redirect(route('logos.index'));
        }

        return view('logos.show')->with('logo', $logo);
    }

    /**
     * Show the form for editing the specified logo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            Flash::error('Logo not found');

            return redirect(route('logos.index'));
        }

        return view('logos.edit')->with('logo', $logo);
    }

    /**
     * Update the specified logo in storage.
     *
     * @param  int              $id
     * @param UpdatelogoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatelogoRequest $request)
    {
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            Flash::error('Logo not found');

            return redirect(route('logos.index'));
        }

        $input = $request->all();

        $file=$request->file('logo_image');
        if($file) {
            $filename= $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $file->move(public_path('uploads/logo'), $filename);
            $input['logo_image'] = $filename;    
        }


        $logo = $this->logoRepository->update($input, $id);

        Flash::success('Logo updated successfully.');

        return redirect(route('logos.index'));
    }

    /**
     * Remove the specified logo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $logo = $this->logoRepository->find($id);

        if (empty($logo)) {
            Flash::error('Logo not found');

            return redirect(route('logos.index'));
        }

        $this->logoRepository->delete($id);

        Flash::success('Logo deleted successfully.');

        return redirect(route('logos.index'));
    }
}
