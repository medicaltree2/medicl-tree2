<?php

namespace App\Http\Controllers;

use App\DataTables\university_imagesDataTable;
use App\Http\Requests;
use App\Models\country_universities;
use App\Http\Requests\Createuniversity_imagesRequest;
use App\Http\Requests\Updateuniversity_imagesRequest;
use App\Repositories\university_imagesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class university_imagesController extends AppBaseController
{
    /** @var  university_imagesRepository */
    private $universityImagesRepository;

    public function __construct(university_imagesRepository $universityImagesRepo)
    {
        $this->universityImagesRepository = $universityImagesRepo;
    }

    /**
     * Display a listing of the university_images.
     *
     * @param university_imagesDataTable $universityImagesDataTable
     * @return Response
     */
    public function index(university_imagesDataTable $universityImagesDataTable)
    {
        return $universityImagesDataTable->render('university_images.index');
    }

    /**
     * Show the form for creating a new university_images.
     *
     * @return Response
     */
    public function create()
    {
        $categories = country_universities::orderBy('id')->pluck('university_name', 'id');
        $data = $categories->toArray();

        return view('university_images.create')->with('university', $data);
    }

    /**
     * Store a newly created university_images in storage.
     *
     * @param Createuniversity_imagesRequest $request
     *
     * @return Response
     */
    public function store(Createuniversity_imagesRequest $request)
    {
        $input = $request->all();

        $file=$request->file('image');
        $filename= $file->getClientOriginalName();
        $filename = str_replace(' ', '_', $filename);
        $file->move(public_path('uploads/university'), $filename);
        $input['image'] = $filename;

        $universityImages = $this->universityImagesRepository->create($input);

        Flash::success('University Images saved successfully.');

        return redirect(route('universityImages.index'));
    }

    /**
     * Display the specified university_images.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            Flash::error('University Images not found');

            return redirect(route('universityImages.index'));
        }

        return view('university_images.show')->with('universityImages', $universityImages);
    }

    /**
     * Show the form for editing the specified university_images.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            Flash::error('University Images not found');

            return redirect(route('universityImages.index'));
        }

        $categories = country_universities::orderBy('id')->pluck('university_name', 'id');
        $data = $categories->toArray();

        return view('university_images.edit')->with(array('university' => $data, 'universityImages' => $universityImages));
    }

    /**
     * Update the specified university_images in storage.
     *
     * @param  int              $id
     * @param Updateuniversity_imagesRequest $request
     *
     * @return Response
     */
    public function update($id, Updateuniversity_imagesRequest $request)
    {
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            Flash::error('University Images not found');

            return redirect(route('universityImages.index'));
        }

        $input = $request->all();

        $file=$request->file('image');
        if($file) {
            $filename= $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $file->move(public_path('uploads/university'), $filename);
            $input['image'] = $filename;
    
        }

        $universityImages = $this->universityImagesRepository->update($input, $id);

        Flash::success('University Images updated successfully.');

        return redirect(route('universityImages.index'));
    }

    /**
     * Remove the specified university_images from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $universityImages = $this->universityImagesRepository->find($id);

        if (empty($universityImages)) {
            Flash::error('University Images not found');

            return redirect(route('universityImages.index'));
        }

        $this->universityImagesRepository->delete($id);

        Flash::success('University Images deleted successfully.');

        return redirect(route('universityImages.index'));
    }
}
