<?php

namespace App\Http\Controllers;

use App\DataTables\country_nameDataTable;
use App\Http\Requests;
use App\Http\Requests\Createcountry_nameRequest;
use App\Http\Requests\Updatecountry_nameRequest;
use App\Repositories\country_nameRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class country_nameController extends AppBaseController
{
    /** @var  country_nameRepository */
    private $countryNameRepository;

    public function __construct(country_nameRepository $countryNameRepo)
    {
        $this->countryNameRepository = $countryNameRepo;
    }

    /**
     * Display a listing of the country_name.
     *
     * @param country_nameDataTable $countryNameDataTable
     * @return Response
     */
    public function index(country_nameDataTable $countryNameDataTable)
    {
        return $countryNameDataTable->render('country_names.index');
    }

    /**
     * Show the form for creating a new country_name.
     *
     * @return Response
     */
    public function create()
    {
        return view('country_names.create');
    }

    /**
     * Store a newly created country_name in storage.
     *
     * @param Createcountry_nameRequest $request
     *
     * @return Response
     */
    public function store(Createcountry_nameRequest $request)
    {
        $input = $request->all();

        $file=$request->file('image');
        $filename= $file->getClientOriginalName();
        $filename = str_replace(' ', '_', $filename);
        $file->move(public_path('uploads/countries'), $filename);
        $input['image'] = $filename;

        $countryName = $this->countryNameRepository->create($input);

        Flash::success('Country Name saved successfully.');

        return redirect(route('countryNames.index'));
    }

    /**
     * Display the specified country_name.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            Flash::error('Country Name not found');

            return redirect(route('countryNames.index'));
        }

        return view('country_names.show')->with('countryName', $countryName);
    }

    /**
     * Show the form for editing the specified country_name.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            Flash::error('Country Name not found');

            return redirect(route('countryNames.index'));
        }

        return view('country_names.edit')->with('countryName', $countryName);
    }

    /**
     * Update the specified country_name in storage.
     *
     * @param  int              $id
     * @param Updatecountry_nameRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecountry_nameRequest $request)
    {
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            Flash::error('Country Name not found');

            return redirect(route('countryNames.index'));
        }

        $input = $request->all();

        $file=$request->file('image');
        if($file) {
            $filename= $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $file->move(public_path('uploads/countries'), $filename);
            $input['image'] = $filename;    
        }

        $countryName = $this->countryNameRepository->update($input, $id);

        Flash::success('Country Name updated successfully.');

        return redirect(route('countryNames.index'));
    }

    /**
     * Remove the specified country_name from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $countryName = $this->countryNameRepository->find($id);

        if (empty($countryName)) {
            Flash::error('Country Name not found');

            return redirect(route('countryNames.index'));
        }

        $this->countryNameRepository->delete($id);

        Flash::success('Country Name deleted successfully.');

        return redirect(route('countryNames.index'));
    }
}
