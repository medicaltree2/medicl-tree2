<?php

namespace App\Http\Controllers;

use App\DataTables\country_universitiesDataTable;
use App\Http\Requests;
use App\Models\country_name;
use App\Http\Requests\Createcountry_universitiesRequest;
use App\Http\Requests\Updatecountry_universitiesRequest;
use App\Repositories\country_universitiesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class country_universitiesController extends AppBaseController
{
    /** @var  country_universitiesRepository */
    private $countryUniversitiesRepository;

    public function __construct(country_universitiesRepository $countryUniversitiesRepo)
    {
        $this->countryUniversitiesRepository = $countryUniversitiesRepo;
    }

    /**
     * Display a listing of the country_universities.
     *
     * @param country_universitiesDataTable $countryUniversitiesDataTable
     * @return Response
     */
    public function index(country_universitiesDataTable $countryUniversitiesDataTable)
    {
        return $countryUniversitiesDataTable->render('country_universities.index');
    }

    /**
     * Show the form for creating a new country_universities.
     *
     * @return Response
     */
    public function create()
    {
        $categories = country_name::orderBy('id')->pluck('country', 'id');
        $data = $categories->toArray();

        return view('country_universities.create')->with('country', $data);
//        return view('country_universities.create');
    }

    /**
     * Store a newly created country_universities in storage.
     *
     * @param Createcountry_universitiesRequest $request
     *
     * @return Response
     */
    public function store(Createcountry_universitiesRequest $request)
    {
        $input = $request->all();

        $file=$request->file('university_images');
        $filename= $file->getClientOriginalName();
        $filename = str_replace(' ', '_', $filename);
        $file->move(public_path('uploads/country_university'), $filename);
        $input['university_images'] = $filename;

        $countryUniversities = $this->countryUniversitiesRepository->create($input);

        Flash::success('Country Universities saved successfully.');

        return redirect(route('countryUniversities.index'));
    }

    /**
     * Display the specified country_universities.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            Flash::error('Country Universities not found');

            return redirect(route('countryUniversities.index'));
        }

        return view('country_universities.show')->with('countryUniversities', $countryUniversities);
    }

    /**
     * Show the form for editing the specified country_universities.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            Flash::error('Country Universities not found');

            return redirect(route('countryUniversities.index'));
        }

        return view('country_universities.edit')->with('countryUniversities', $countryUniversities);
    }

    /**
     * Update the specified country_universities in storage.
     *
     * @param  int              $id
     * @param Updatecountry_universitiesRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecountry_universitiesRequest $request)
    {
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            Flash::error('Country Universities not found');

            return redirect(route('countryUniversities.index'));
        }

        $input = $request->all();

        $file=$request->file('university_images');
        if($file) {
            $filename= $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $file->move(public_path('uploads/country_university'), $filename);
            $input['university_images'] = $filename;    
        }

        $countryUniversities = $this->countryUniversitiesRepository->update($input, $id);

        Flash::success('Country Universities updated successfully.');

        return redirect(route('countryUniversities.index'));
    }

    /**
     * Remove the specified country_universities from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $countryUniversities = $this->countryUniversitiesRepository->find($id);

        if (empty($countryUniversities)) {
            Flash::error('Country Universities not found');

            return redirect(route('countryUniversities.index'));
        }

        $this->countryUniversitiesRepository->delete($id);

        Flash::success('Country Universities deleted successfully.');

        return redirect(route('countryUniversities.index'));
    }
}
