<?php

namespace App\Http\Controllers;

use App\DataTables\university_infoDataTable;
use App\Http\Requests;
use App\Models\country_universities;
use App\Http\Requests\Createuniversity_infoRequest;
use App\Http\Requests\Updateuniversity_infoRequest;
use App\Repositories\university_infoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class university_infoController extends AppBaseController
{
    /** @var  university_infoRepository */
    private $universityInfoRepository;

    public function __construct(university_infoRepository $universityInfoRepo)
    {
        $this->universityInfoRepository = $universityInfoRepo;
    }

    /**
     * Display a listing of the university_info.
     *
     * @param university_infoDataTable $universityInfoDataTable
     * @return Response
     */
    public function index(university_infoDataTable $universityInfoDataTable)
    {
        return $universityInfoDataTable->render('university_infos.index');
    }

    /**
     * Show the form for creating a new university_info.
     *
     * @return Response
     */
    public function create()
    {
        $categories = country_universities::orderBy('id')->pluck('university_name', 'id');
        $data = $categories->toArray();

        return view('university_infos.create')->with('university', $data);
    }

    /**
     * Store a newly created university_info in storage.
     *
     * @param Createuniversity_infoRequest $request
     *
     * @return Response
     */
    public function store(Createuniversity_infoRequest $request)
    {
        $input = $request->all();

        $universityInfo = $this->universityInfoRepository->create($input);

        Flash::success('University Info saved successfully.');

        return redirect(route('universityInfos.index'));
    }

    /**
     * Display the specified university_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            Flash::error('University Info not found');

            return redirect(route('universityInfos.index'));
        }

        return view('university_infos.show')->with('universityInfo', $universityInfo);
    }

    /**
     * Show the form for editing the specified university_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            Flash::error('University Info not found');

            return redirect(route('universityInfos.index'));
        }

        $categories = country_universities::orderBy('id')->pluck('university_name', 'id');
        $data = $categories->toArray();

        return view('university_infos.edit')->with(array('university' => $data, 'universityInfo' => $universityInfo));
    }

    /**
     * Update the specified university_info in storage.
     *
     * @param  int              $id
     * @param Updateuniversity_infoRequest $request
     *
     * @return Response
     */
    public function update($id, Updateuniversity_infoRequest $request)
    {
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            Flash::error('University Info not found');

            return redirect(route('universityInfos.index'));
        }

        $universityInfo = $this->universityInfoRepository->update($request->all(), $id);

        Flash::success('University Info updated successfully.');

        return redirect(route('universityInfos.index'));
    }

    /**
     * Remove the specified university_info from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $universityInfo = $this->universityInfoRepository->find($id);

        if (empty($universityInfo)) {
            Flash::error('University Info not found');

            return redirect(route('universityInfos.index'));
        }

        $this->universityInfoRepository->delete($id);

        Flash::success('University Info deleted successfully.');

        return redirect(route('universityInfos.index'));
    }
}
