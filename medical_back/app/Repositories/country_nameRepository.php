<?php

namespace App\Repositories;

use App\Models\country_name;
use App\Repositories\BaseRepository;

/**
 * Class country_nameRepository
 * @package App\Repositories
 * @version December 12, 2019, 9:45 am UTC
*/

class country_nameRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return country_name::class;
    }
}
