<?php

namespace App\Repositories;

use App\Models\logo;
use App\Repositories\BaseRepository;

/**
 * Class logoRepository
 * @package App\Repositories
 * @version December 23, 2019, 8:33 am UTC
*/

class logoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'logo_image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return logo::class;
    }
}
