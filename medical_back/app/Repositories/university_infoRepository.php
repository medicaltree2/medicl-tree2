<?php

namespace App\Repositories;

use App\Models\university_info;
use App\Repositories\BaseRepository;

/**
 * Class university_infoRepository
 * @package App\Repositories
 * @version December 12, 2019, 10:32 am UTC
*/

class university_infoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'university_id',
        'university_info'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return university_info::class;
    }
}
