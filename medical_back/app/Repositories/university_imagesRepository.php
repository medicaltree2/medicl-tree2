<?php

namespace App\Repositories;

use App\Models\university_images;
use App\Repositories\BaseRepository;

/**
 * Class university_imagesRepository
 * @package App\Repositories
 * @version December 12, 2019, 10:33 am UTC
*/

class university_imagesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'university_id',
        'image',
        'title'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return university_images::class;
    }
}
