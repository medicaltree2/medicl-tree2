<?php

namespace App\Repositories;

use App\Models\about;
use App\Repositories\BaseRepository;

/**
 * Class aboutRepository
 * @package App\Repositories
 * @version December 23, 2019, 2:01 pm UTC
*/

class aboutRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'heading',
        'image',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return about::class;
    }
}
