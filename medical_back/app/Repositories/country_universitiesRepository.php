<?php

namespace App\Repositories;

use App\Models\country_universities;
use App\Repositories\BaseRepository;

/**
 * Class country_universitiesRepository
 * @package App\Repositories
 * @version December 12, 2019, 10:01 am UTC
*/

class country_universitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'university_name',
        'university_images'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return country_universities::class;
    }
}
