<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('country_names', 'country_nameAPIController');

Route::resource('country_universities', 'country_universitiesAPIController');

Route::resource('university_infos', 'university_infoAPIController');

Route::resource('university_images', 'university_imagesAPIController');

Route::resource('banners', 'bannerAPIController');

Route::resource('logos', 'logoAPIController');













Route::resource('abouts', 'aboutAPIController');