<?php namespace Tests\Repositories;

use App\Models\banner;
use App\Repositories\bannerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class bannerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var bannerRepository
     */
    protected $bannerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bannerRepo = \App::make(bannerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_banner()
    {
        $banner = factory(banner::class)->make()->toArray();

        $createdbanner = $this->bannerRepo->create($banner);

        $createdbanner = $createdbanner->toArray();
        $this->assertArrayHasKey('id', $createdbanner);
        $this->assertNotNull($createdbanner['id'], 'Created banner must have id specified');
        $this->assertNotNull(banner::find($createdbanner['id']), 'banner with given id must be in DB');
        $this->assertModelData($banner, $createdbanner);
    }

    /**
     * @test read
     */
    public function test_read_banner()
    {
        $banner = factory(banner::class)->create();

        $dbbanner = $this->bannerRepo->find($banner->id);

        $dbbanner = $dbbanner->toArray();
        $this->assertModelData($banner->toArray(), $dbbanner);
    }

    /**
     * @test update
     */
    public function test_update_banner()
    {
        $banner = factory(banner::class)->create();
        $fakebanner = factory(banner::class)->make()->toArray();

        $updatedbanner = $this->bannerRepo->update($fakebanner, $banner->id);

        $this->assertModelData($fakebanner, $updatedbanner->toArray());
        $dbbanner = $this->bannerRepo->find($banner->id);
        $this->assertModelData($fakebanner, $dbbanner->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_banner()
    {
        $banner = factory(banner::class)->create();

        $resp = $this->bannerRepo->delete($banner->id);

        $this->assertTrue($resp);
        $this->assertNull(banner::find($banner->id), 'banner should not exist in DB');
    }
}
