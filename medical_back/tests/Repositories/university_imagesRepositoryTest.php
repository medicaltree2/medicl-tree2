<?php namespace Tests\Repositories;

use App\Models\university_images;
use App\Repositories\university_imagesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class university_imagesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var university_imagesRepository
     */
    protected $universityImagesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->universityImagesRepo = \App::make(university_imagesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_university_images()
    {
        $universityImages = factory(university_images::class)->make()->toArray();

        $createduniversity_images = $this->universityImagesRepo->create($universityImages);

        $createduniversity_images = $createduniversity_images->toArray();
        $this->assertArrayHasKey('id', $createduniversity_images);
        $this->assertNotNull($createduniversity_images['id'], 'Created university_images must have id specified');
        $this->assertNotNull(university_images::find($createduniversity_images['id']), 'university_images with given id must be in DB');
        $this->assertModelData($universityImages, $createduniversity_images);
    }

    /**
     * @test read
     */
    public function test_read_university_images()
    {
        $universityImages = factory(university_images::class)->create();

        $dbuniversity_images = $this->universityImagesRepo->find($universityImages->id);

        $dbuniversity_images = $dbuniversity_images->toArray();
        $this->assertModelData($universityImages->toArray(), $dbuniversity_images);
    }

    /**
     * @test update
     */
    public function test_update_university_images()
    {
        $universityImages = factory(university_images::class)->create();
        $fakeuniversity_images = factory(university_images::class)->make()->toArray();

        $updateduniversity_images = $this->universityImagesRepo->update($fakeuniversity_images, $universityImages->id);

        $this->assertModelData($fakeuniversity_images, $updateduniversity_images->toArray());
        $dbuniversity_images = $this->universityImagesRepo->find($universityImages->id);
        $this->assertModelData($fakeuniversity_images, $dbuniversity_images->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_university_images()
    {
        $universityImages = factory(university_images::class)->create();

        $resp = $this->universityImagesRepo->delete($universityImages->id);

        $this->assertTrue($resp);
        $this->assertNull(university_images::find($universityImages->id), 'university_images should not exist in DB');
    }
}
