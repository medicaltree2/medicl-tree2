<?php namespace Tests\Repositories;

use App\Models\about;
use App\Repositories\aboutRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class aboutRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var aboutRepository
     */
    protected $aboutRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->aboutRepo = \App::make(aboutRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_about()
    {
        $about = factory(about::class)->make()->toArray();

        $createdabout = $this->aboutRepo->create($about);

        $createdabout = $createdabout->toArray();
        $this->assertArrayHasKey('id', $createdabout);
        $this->assertNotNull($createdabout['id'], 'Created about must have id specified');
        $this->assertNotNull(about::find($createdabout['id']), 'about with given id must be in DB');
        $this->assertModelData($about, $createdabout);
    }

    /**
     * @test read
     */
    public function test_read_about()
    {
        $about = factory(about::class)->create();

        $dbabout = $this->aboutRepo->find($about->id);

        $dbabout = $dbabout->toArray();
        $this->assertModelData($about->toArray(), $dbabout);
    }

    /**
     * @test update
     */
    public function test_update_about()
    {
        $about = factory(about::class)->create();
        $fakeabout = factory(about::class)->make()->toArray();

        $updatedabout = $this->aboutRepo->update($fakeabout, $about->id);

        $this->assertModelData($fakeabout, $updatedabout->toArray());
        $dbabout = $this->aboutRepo->find($about->id);
        $this->assertModelData($fakeabout, $dbabout->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_about()
    {
        $about = factory(about::class)->create();

        $resp = $this->aboutRepo->delete($about->id);

        $this->assertTrue($resp);
        $this->assertNull(about::find($about->id), 'about should not exist in DB');
    }
}
