<?php namespace Tests\Repositories;

use App\Models\university_info;
use App\Repositories\university_infoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class university_infoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var university_infoRepository
     */
    protected $universityInfoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->universityInfoRepo = \App::make(university_infoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_university_info()
    {
        $universityInfo = factory(university_info::class)->make()->toArray();

        $createduniversity_info = $this->universityInfoRepo->create($universityInfo);

        $createduniversity_info = $createduniversity_info->toArray();
        $this->assertArrayHasKey('id', $createduniversity_info);
        $this->assertNotNull($createduniversity_info['id'], 'Created university_info must have id specified');
        $this->assertNotNull(university_info::find($createduniversity_info['id']), 'university_info with given id must be in DB');
        $this->assertModelData($universityInfo, $createduniversity_info);
    }

    /**
     * @test read
     */
    public function test_read_university_info()
    {
        $universityInfo = factory(university_info::class)->create();

        $dbuniversity_info = $this->universityInfoRepo->find($universityInfo->id);

        $dbuniversity_info = $dbuniversity_info->toArray();
        $this->assertModelData($universityInfo->toArray(), $dbuniversity_info);
    }

    /**
     * @test update
     */
    public function test_update_university_info()
    {
        $universityInfo = factory(university_info::class)->create();
        $fakeuniversity_info = factory(university_info::class)->make()->toArray();

        $updateduniversity_info = $this->universityInfoRepo->update($fakeuniversity_info, $universityInfo->id);

        $this->assertModelData($fakeuniversity_info, $updateduniversity_info->toArray());
        $dbuniversity_info = $this->universityInfoRepo->find($universityInfo->id);
        $this->assertModelData($fakeuniversity_info, $dbuniversity_info->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_university_info()
    {
        $universityInfo = factory(university_info::class)->create();

        $resp = $this->universityInfoRepo->delete($universityInfo->id);

        $this->assertTrue($resp);
        $this->assertNull(university_info::find($universityInfo->id), 'university_info should not exist in DB');
    }
}
