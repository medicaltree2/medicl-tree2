<?php namespace Tests\Repositories;

use App\Models\country_universities;
use App\Repositories\country_universitiesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class country_universitiesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var country_universitiesRepository
     */
    protected $countryUniversitiesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->countryUniversitiesRepo = \App::make(country_universitiesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->make()->toArray();

        $createdcountry_universities = $this->countryUniversitiesRepo->create($countryUniversities);

        $createdcountry_universities = $createdcountry_universities->toArray();
        $this->assertArrayHasKey('id', $createdcountry_universities);
        $this->assertNotNull($createdcountry_universities['id'], 'Created country_universities must have id specified');
        $this->assertNotNull(country_universities::find($createdcountry_universities['id']), 'country_universities with given id must be in DB');
        $this->assertModelData($countryUniversities, $createdcountry_universities);
    }

    /**
     * @test read
     */
    public function test_read_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->create();

        $dbcountry_universities = $this->countryUniversitiesRepo->find($countryUniversities->id);

        $dbcountry_universities = $dbcountry_universities->toArray();
        $this->assertModelData($countryUniversities->toArray(), $dbcountry_universities);
    }

    /**
     * @test update
     */
    public function test_update_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->create();
        $fakecountry_universities = factory(country_universities::class)->make()->toArray();

        $updatedcountry_universities = $this->countryUniversitiesRepo->update($fakecountry_universities, $countryUniversities->id);

        $this->assertModelData($fakecountry_universities, $updatedcountry_universities->toArray());
        $dbcountry_universities = $this->countryUniversitiesRepo->find($countryUniversities->id);
        $this->assertModelData($fakecountry_universities, $dbcountry_universities->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->create();

        $resp = $this->countryUniversitiesRepo->delete($countryUniversities->id);

        $this->assertTrue($resp);
        $this->assertNull(country_universities::find($countryUniversities->id), 'country_universities should not exist in DB');
    }
}
