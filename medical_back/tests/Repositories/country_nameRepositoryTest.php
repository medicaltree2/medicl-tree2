<?php namespace Tests\Repositories;

use App\Models\country_name;
use App\Repositories\country_nameRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class country_nameRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var country_nameRepository
     */
    protected $countryNameRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->countryNameRepo = \App::make(country_nameRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_country_name()
    {
        $countryName = factory(country_name::class)->make()->toArray();

        $createdcountry_name = $this->countryNameRepo->create($countryName);

        $createdcountry_name = $createdcountry_name->toArray();
        $this->assertArrayHasKey('id', $createdcountry_name);
        $this->assertNotNull($createdcountry_name['id'], 'Created country_name must have id specified');
        $this->assertNotNull(country_name::find($createdcountry_name['id']), 'country_name with given id must be in DB');
        $this->assertModelData($countryName, $createdcountry_name);
    }

    /**
     * @test read
     */
    public function test_read_country_name()
    {
        $countryName = factory(country_name::class)->create();

        $dbcountry_name = $this->countryNameRepo->find($countryName->id);

        $dbcountry_name = $dbcountry_name->toArray();
        $this->assertModelData($countryName->toArray(), $dbcountry_name);
    }

    /**
     * @test update
     */
    public function test_update_country_name()
    {
        $countryName = factory(country_name::class)->create();
        $fakecountry_name = factory(country_name::class)->make()->toArray();

        $updatedcountry_name = $this->countryNameRepo->update($fakecountry_name, $countryName->id);

        $this->assertModelData($fakecountry_name, $updatedcountry_name->toArray());
        $dbcountry_name = $this->countryNameRepo->find($countryName->id);
        $this->assertModelData($fakecountry_name, $dbcountry_name->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_country_name()
    {
        $countryName = factory(country_name::class)->create();

        $resp = $this->countryNameRepo->delete($countryName->id);

        $this->assertTrue($resp);
        $this->assertNull(country_name::find($countryName->id), 'country_name should not exist in DB');
    }
}
