<?php namespace Tests\Repositories;

use App\Models\logo;
use App\Repositories\logoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class logoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var logoRepository
     */
    protected $logoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->logoRepo = \App::make(logoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_logo()
    {
        $logo = factory(logo::class)->make()->toArray();

        $createdlogo = $this->logoRepo->create($logo);

        $createdlogo = $createdlogo->toArray();
        $this->assertArrayHasKey('id', $createdlogo);
        $this->assertNotNull($createdlogo['id'], 'Created logo must have id specified');
        $this->assertNotNull(logo::find($createdlogo['id']), 'logo with given id must be in DB');
        $this->assertModelData($logo, $createdlogo);
    }

    /**
     * @test read
     */
    public function test_read_logo()
    {
        $logo = factory(logo::class)->create();

        $dblogo = $this->logoRepo->find($logo->id);

        $dblogo = $dblogo->toArray();
        $this->assertModelData($logo->toArray(), $dblogo);
    }

    /**
     * @test update
     */
    public function test_update_logo()
    {
        $logo = factory(logo::class)->create();
        $fakelogo = factory(logo::class)->make()->toArray();

        $updatedlogo = $this->logoRepo->update($fakelogo, $logo->id);

        $this->assertModelData($fakelogo, $updatedlogo->toArray());
        $dblogo = $this->logoRepo->find($logo->id);
        $this->assertModelData($fakelogo, $dblogo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_logo()
    {
        $logo = factory(logo::class)->create();

        $resp = $this->logoRepo->delete($logo->id);

        $this->assertTrue($resp);
        $this->assertNull(logo::find($logo->id), 'logo should not exist in DB');
    }
}
