<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\about;

class aboutApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_about()
    {
        $about = factory(about::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/abouts', $about
        );

        $this->assertApiResponse($about);
    }

    /**
     * @test
     */
    public function test_read_about()
    {
        $about = factory(about::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/abouts/'.$about->id
        );

        $this->assertApiResponse($about->toArray());
    }

    /**
     * @test
     */
    public function test_update_about()
    {
        $about = factory(about::class)->create();
        $editedabout = factory(about::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/abouts/'.$about->id,
            $editedabout
        );

        $this->assertApiResponse($editedabout);
    }

    /**
     * @test
     */
    public function test_delete_about()
    {
        $about = factory(about::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/abouts/'.$about->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/abouts/'.$about->id
        );

        $this->response->assertStatus(404);
    }
}
