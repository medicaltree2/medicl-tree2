<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\university_images;

class university_imagesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_university_images()
    {
        $universityImages = factory(university_images::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/university_images', $universityImages
        );

        $this->assertApiResponse($universityImages);
    }

    /**
     * @test
     */
    public function test_read_university_images()
    {
        $universityImages = factory(university_images::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/university_images/'.$universityImages->id
        );

        $this->assertApiResponse($universityImages->toArray());
    }

    /**
     * @test
     */
    public function test_update_university_images()
    {
        $universityImages = factory(university_images::class)->create();
        $editeduniversity_images = factory(university_images::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/university_images/'.$universityImages->id,
            $editeduniversity_images
        );

        $this->assertApiResponse($editeduniversity_images);
    }

    /**
     * @test
     */
    public function test_delete_university_images()
    {
        $universityImages = factory(university_images::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/university_images/'.$universityImages->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/university_images/'.$universityImages->id
        );

        $this->response->assertStatus(404);
    }
}
