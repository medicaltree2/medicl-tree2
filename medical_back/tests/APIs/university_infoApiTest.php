<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\university_info;

class university_infoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_university_info()
    {
        $universityInfo = factory(university_info::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/university_infos', $universityInfo
        );

        $this->assertApiResponse($universityInfo);
    }

    /**
     * @test
     */
    public function test_read_university_info()
    {
        $universityInfo = factory(university_info::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/university_infos/'.$universityInfo->id
        );

        $this->assertApiResponse($universityInfo->toArray());
    }

    /**
     * @test
     */
    public function test_update_university_info()
    {
        $universityInfo = factory(university_info::class)->create();
        $editeduniversity_info = factory(university_info::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/university_infos/'.$universityInfo->id,
            $editeduniversity_info
        );

        $this->assertApiResponse($editeduniversity_info);
    }

    /**
     * @test
     */
    public function test_delete_university_info()
    {
        $universityInfo = factory(university_info::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/university_infos/'.$universityInfo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/university_infos/'.$universityInfo->id
        );

        $this->response->assertStatus(404);
    }
}
