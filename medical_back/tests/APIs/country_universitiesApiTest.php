<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\country_universities;

class country_universitiesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/country_universities', $countryUniversities
        );

        $this->assertApiResponse($countryUniversities);
    }

    /**
     * @test
     */
    public function test_read_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/country_universities/'.$countryUniversities->id
        );

        $this->assertApiResponse($countryUniversities->toArray());
    }

    /**
     * @test
     */
    public function test_update_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->create();
        $editedcountry_universities = factory(country_universities::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/country_universities/'.$countryUniversities->id,
            $editedcountry_universities
        );

        $this->assertApiResponse($editedcountry_universities);
    }

    /**
     * @test
     */
    public function test_delete_country_universities()
    {
        $countryUniversities = factory(country_universities::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/country_universities/'.$countryUniversities->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/country_universities/'.$countryUniversities->id
        );

        $this->response->assertStatus(404);
    }
}
