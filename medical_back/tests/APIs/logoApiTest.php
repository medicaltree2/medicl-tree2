<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\logo;

class logoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_logo()
    {
        $logo = factory(logo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/logos', $logo
        );

        $this->assertApiResponse($logo);
    }

    /**
     * @test
     */
    public function test_read_logo()
    {
        $logo = factory(logo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/logos/'.$logo->id
        );

        $this->assertApiResponse($logo->toArray());
    }

    /**
     * @test
     */
    public function test_update_logo()
    {
        $logo = factory(logo::class)->create();
        $editedlogo = factory(logo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/logos/'.$logo->id,
            $editedlogo
        );

        $this->assertApiResponse($editedlogo);
    }

    /**
     * @test
     */
    public function test_delete_logo()
    {
        $logo = factory(logo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/logos/'.$logo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/logos/'.$logo->id
        );

        $this->response->assertStatus(404);
    }
}
