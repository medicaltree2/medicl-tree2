<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\country_name;

class country_nameApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_country_name()
    {
        $countryName = factory(country_name::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/country_names', $countryName
        );

        $this->assertApiResponse($countryName);
    }

    /**
     * @test
     */
    public function test_read_country_name()
    {
        $countryName = factory(country_name::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/country_names/'.$countryName->id
        );

        $this->assertApiResponse($countryName->toArray());
    }

    /**
     * @test
     */
    public function test_update_country_name()
    {
        $countryName = factory(country_name::class)->create();
        $editedcountry_name = factory(country_name::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/country_names/'.$countryName->id,
            $editedcountry_name
        );

        $this->assertApiResponse($editedcountry_name);
    }

    /**
     * @test
     */
    public function test_delete_country_name()
    {
        $countryName = factory(country_name::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/country_names/'.$countryName->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/country_names/'.$countryName->id
        );

        $this->response->assertStatus(404);
    }
}
