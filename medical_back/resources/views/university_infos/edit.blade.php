@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            University Info
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($universityInfo, ['route' => ['universityInfos.update', $universityInfo->id], 'method' => 'patch']) !!}

                        @include('university_infos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection