<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $universityInfo->id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $universityInfo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $universityInfo->updated_at }}</p>
</div>

<!-- University Id Field -->
<div class="form-group">
    {!! Form::label('university_id', 'University Id:') !!}
    <p>{{ $universityInfo->university_id }}</p>
</div>

<!-- University Info Field -->
<div class="form-group">
    {!! Form::label('university_info', 'University Info:') !!}
    <p>{{ $universityInfo->university_info }}</p>
</div>

