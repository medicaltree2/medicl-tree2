<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $logo->id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $logo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $logo->updated_at }}</p>
</div>

<!-- Logo Image Field -->
<div class="form-group">
    {!! Form::label('logo_image', 'Logo Image:') !!}
    <p>{{ $logo->logo_image }}</p>
</div>

