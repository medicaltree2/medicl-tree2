<!-- Logo Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo_image', 'Logo Image:') !!}
    {!! Form::file('logo_image') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('logos.index') }}" class="btn btn-default">Cancel</a>
</div>
