<li class="{{ Request::is('countryNames*') ? 'active' : '' }}">
    <a href="{{ route('countryNames.index') }}"><i class="fa fa-edit"></i><span>Country Names</span></a>
</li>

<li class="{{ Request::is('countryUniversities*') ? 'active' : '' }}">
    <a href="{{ route('countryUniversities.index') }}"><i class="fa fa-edit"></i><span>Country Universities</span></a>
</li>

<li class="{{ Request::is('universityInfos*') ? 'active' : '' }}">
    <a href="{{ route('universityInfos.index') }}"><i class="fa fa-edit"></i><span>University Infos</span></a>
</li>

<li class="{{ Request::is('universityImages*') ? 'active' : '' }}">
    <a href="{{ route('universityImages.index') }}"><i class="fa fa-edit"></i><span>University Images</span></a>
</li>

<li class="{{ Request::is('banners*') ? 'active' : '' }}">
    <a href="{{ route('banners.index') }}"><i class="fa fa-edit"></i><span>Banners</span></a>
</li>

<li class="{{ Request::is('logos*') ? 'active' : '' }}">
    <a href="{{ route('logos.index') }}"><i class="fa fa-edit"></i><span>Logos</span></a>
</li>







<li class="{{ Request::is('abouts*') ? 'active' : '' }}">
    <a href="{{ route('abouts.index') }}"><i class="fa fa-edit"></i><span>Abouts</span></a>
</li>

