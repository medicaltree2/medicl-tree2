@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Country Universities
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($countryUniversities, ['route' => ['countryUniversities.update', $countryUniversities->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('country_universities.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection