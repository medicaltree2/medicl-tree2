<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $countryUniversities->id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $countryUniversities->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $countryUniversities->updated_at }}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{{ $countryUniversities->country_id }}</p>
</div>

<!-- University Name Field -->
<div class="form-group">
    {!! Form::label('university_name', 'University Name:') !!}
    <p>{{ $countryUniversities->university_name }}</p>
</div>

<!-- University Images Field -->
<div class="form-group">
    {!! Form::label('university_images', 'University Images:') !!}
    <p>{{ $countryUniversities->university_images }}</p>
</div>

