<!-- Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country:') !!}
    {!! Form::select('country_id', $country , null, ['class' => 'form-control']) !!}
</div>

<!-- University Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('university_name', 'University Name:') !!}
    {!! Form::text('university_name', null, ['class' => 'form-control']) !!}
</div>

<!-- University Images Field -->
<div class="form-group col-sm-6">
    {!! Form::label('university_images', 'University Images:') !!}
    {!! Form::file('university_images') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('countryUniversities.index') }}" class="btn btn-default">Cancel</a>
</div>
