<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $banner->id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $banner->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $banner->updated_at }}</p>
</div>

<!-- Banner Image Field -->
<div class="form-group">
    {!! Form::label('banner_image', 'Banner Image:') !!}
    <p>{{ $banner->banner_image }}</p>
</div>

<!-- Banner Caption Field -->
<div class="form-group">
    {!! Form::label('banner_caption', 'Banner Caption:') !!}
    <p>{{ $banner->banner_caption }}</p>
</div>

