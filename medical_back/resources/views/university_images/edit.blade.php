@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            University Images
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($universityImages, ['route' => ['universityImages.update', $universityImages->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('university_images.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection