@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Country Name
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($countryName, ['route' => ['countryNames.update', $countryName->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('country_names.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection