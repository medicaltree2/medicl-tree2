<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\aboutus;
use Faker\Generator as Faker;

$factory->define(aboutus::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'heading' => $faker->text,
        'image' => $faker->text,
        'description' => $faker->text
    ];
});
