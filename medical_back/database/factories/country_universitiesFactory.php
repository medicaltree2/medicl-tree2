<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\country_universities;
use Faker\Generator as Faker;

$factory->define(country_universities::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'country_id' => $faker->randomDigitNotNull,
        'university_name' => $faker->text,
        'university_images' => $faker->text
    ];
});
