<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\about_us;
use Faker\Generator as Faker;

$factory->define(about_us::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'heading' => $faker->text,
        'image' => $faker->text,
        'description' => $faker->word
    ];
});
