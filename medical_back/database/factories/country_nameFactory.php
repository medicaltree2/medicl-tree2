<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\country_name;
use Faker\Generator as Faker;

$factory->define(country_name::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'country' => $faker->text,
        'image' => $faker->text
    ];
});
