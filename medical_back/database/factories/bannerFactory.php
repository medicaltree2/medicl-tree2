<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\banner;
use Faker\Generator as Faker;

$factory->define(banner::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'banner_image' => $faker->text,
        'banner_caption' => $faker->text
    ];
});
