<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\university_images;
use Faker\Generator as Faker;

$factory->define(university_images::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'university_id' => $faker->word,
        'image' => $faker->text,
        'title' => $faker->word
    ];
});
