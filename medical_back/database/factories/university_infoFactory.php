<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\university_info;
use Faker\Generator as Faker;

$factory->define(university_info::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'university_id' => $faker->randomDigitNotNull,
        'university_info' => $faker->word
    ];
});
